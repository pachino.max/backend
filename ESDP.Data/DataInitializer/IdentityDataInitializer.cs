﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using User = ESDP.Data.Entities.User;

namespace ESDP.Data.DataInitializer
{
    public static class IdentityDataInitializer
    {
        private const string ADMINROLE = "Administrator";
        private const string DIRECTORROLE = "Director";
        private const string HEADTEACHERROLE = "Headteacher";
        private const string TEACHERROLE = "Teacher";
        public static void SeedData(UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        public static void SeedUsers(UserManager<User> userManager)
        {
            Dictionary<string, string> admin = new Dictionary<string, string>()
            {
                {"Name", "Admin" },
                {"Email", "Admin@test" },
                {"Password", "Asd123" },
                {"Role", ADMINROLE },
                {"Surname", "Admin" }
            };

            Dictionary<string, string> teacher1 = new Dictionary<string, string>()
            {
                {"Name", "Teacher1" },
                {"Email", "Teacher1@test" },
                {"Password", "Asd123" },
                {"Role", TEACHERROLE },
                {"Surname", "Teacher" }
            };

            List<Dictionary<string, string>> users = new List<Dictionary<string, string>>
            {
                admin,
                teacher1
            };

            AddSeedUsers(users, userManager);

        }

        private static void AddSeedUsers(List<Dictionary<string, string>> users, UserManager<User> userManager)
        {
            foreach (var item in users)
            {
                if (userManager.FindByNameAsync
                        (item["Name"]).Result == null)
                {
                    User user = new User();
                    user.UserName = item["Name"];
                    user.Email = item["Email"];
                    user.Name = item["Name"];
                    user.Surname = item["Surname"];

                    IdentityResult result = userManager.CreateAsync
                        (user, item["Password"]).Result;

                    if (result.Succeeded)
                    {
                        userManager.AddToRoleAsync(user,
                            item["Role"]).Wait();
                    }
                }
            }
        }

        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            List<string> roles = new List<string>
            {
                ADMINROLE,
                DIRECTORROLE,
                HEADTEACHERROLE,
                TEACHERROLE
            };

            AddSeedRoles(roleManager, roles);
        }

        private static void AddSeedRoles(RoleManager<IdentityRole> roleManager, List<string> roles)
        {
            foreach (var item in roles)
            {
                if (!roleManager.RoleExistsAsync
                    (item).Result)
                {
                    IdentityRole role = new IdentityRole();
                    role.Name = item;
                    IdentityResult roleResult = roleManager.
                        CreateAsync(role).Result;
                }
            }

        }


    }
}
