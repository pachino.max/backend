﻿using ESDP.Common.Dtos.ClassDataDtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Common.Dtos.AcademicClassDtos
{
    public class AcademicClassDto : BaseEntityDto
    {
        [Required]
        public int YearId { get; set; }
        [Required]
        public string ClassroomTeacherId { get; set; }
        [Required]
        public int IdentifierClassId { get; set; }
    }
}
