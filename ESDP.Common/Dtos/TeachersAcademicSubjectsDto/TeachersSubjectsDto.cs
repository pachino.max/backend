﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.TeachersAcademicSubjectsDto
{
    public class TeachersSubjectsDto
    {
        public int TeacherSubjectId { get; set; }
        public string SubjectName { get; set; }
    }
}
