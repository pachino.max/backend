﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.GradesDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;

namespace ESDP.BL.Services
{
    public class GradesService
    {
        private readonly IMapper _mapper;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public GradesService(IMapper mapper, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _mapper = mapper;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<OperationResult> ChangeGradesAsync(GradesDto model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var grades = await unitOfWork.GradeRepository.GetByClassDataIdAsync(model.ClassDataId);

                grades.FailingGradesCount = model.FailingGradesCount;
                grades.FiveCount = model.FiveCount;
                grades.FourCount = model.FourCount;
                grades.ThreeCount = model.ThreeCount;
                grades.TwoCount = model.TwoCount;

                unitOfWork.GradeRepository.Update(grades);
                await unitOfWork.CompleteAsync();

                return new OperationResult(true, "Оценки успешно изменены");
            }
        }

        public async Task<OperationResult> AddNewGradesAsync(GradesDto model)
        {
            var grades = _mapper.Map<Grades>(model);
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                await unitOfWork.GradeRepository.CreateAsync(grades);
                await unitOfWork.CompleteAsync();
            }

            return new OperationResult(true, "Оценки успешно добавлены");
        }
    }
}
