﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ESDP.BL.Services;
using ESDP.Common;
using ESDP.Common.Dtos.GradesDtos;
using ESDP.Common.Dtos.TeacherPageDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Teacher")]
    public class TeacherPageController : Controller
    {
        private readonly TeacherPageService _teacherPageService;

        public TeacherPageController(TeacherPageService teacherPageService)
        {
            _teacherPageService = teacherPageService;
        }

        [Route("api/teacherPage")]
        [HttpGet]
        public async Task<ActionResult> GetCurrentTeacherClassTeaching(TeacherPageFilterDto filter) //todo filter by reportPeriodId, subjectId
        {
            var userName = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var teacherPageDto = await _teacherPageService.GetTeachersClassTeachingAsync(userName, filter);
            return Ok(teacherPageDto);
        }

        [Route("api/getGrades/{classDataId}")]
        [HttpGet]
        public async Task<ActionResult> GetGrades(int classDataId)
        {
            var classGrades = await _teacherPageService.GetClassGradesAsync(classDataId);
            return Ok(classGrades);
        }

        [Route("api/addGrades")]
        [HttpPost]
        public async Task<ActionResult> AddGrades([FromBody] GradesDto model)
        {
            if (ModelState.IsValid && model.ClassDataId != 0)
            {
                var result = await _teacherPageService.AddGradesAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }

            return StatusCode(400, new OperationResult(false, "Неверно введены данные"));
        }
    }
}