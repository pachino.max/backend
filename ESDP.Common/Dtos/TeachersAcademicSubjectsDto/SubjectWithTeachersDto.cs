﻿using System;
using System.Collections.Generic;
using System.Text;
using ESDP.Common.Dtos.TeachersDtos;

namespace ESDP.Common.Dtos.TeachersAcademicSubjectsDto
{
    public class SubjectWithTeachersDto
    {
        public int SubjectId { get; set; }

        public string SubjectName { get; set; }

        public List<TeacherDto> Teachers { get; set; }
    }
}
