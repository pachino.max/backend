﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Common.Dtos.AcademicYearDtos
{
    public class AcademicYearDto : BaseEntityDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime? BeginDate { get; set; }
        [Required]
        public DateTime? EndDate { get; set; }
        public bool IsCurrent { get; set; }
    }
}
