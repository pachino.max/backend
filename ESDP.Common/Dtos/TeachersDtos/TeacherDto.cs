﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.TeachersDtos
{
    public class TeacherDto : BaseEntityDto
    {
        public string UserId { get; set; }
        public string Fullname { get; set; }
        public List<int> AcademicSubjectsId { get; set; }
    }
}
