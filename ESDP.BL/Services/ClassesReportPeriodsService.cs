﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.ClassesReportPeriodsDtos;
using ESDP.Common.Dtos.ReportPeriodDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;

namespace ESDP.BL.Services
{
    public class ClassesReportPeriodsService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        private readonly ReportPeriodService _reportPeriodService;

        public ClassesReportPeriodsService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, ReportPeriodService reportPeriodService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
            _reportPeriodService = reportPeriodService;
        }


        public async Task<List<ClassesReportPeriods>> GetClassReportPeriodsByClassIdAsync(int classId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classReportPeriods = await unitOfWork.ClassesReportPeriodsRepository.GetByClassIdIncludingAll(classId);
                return classReportPeriods;
            }
        }

        public async Task<bool> IsExistReportPeriodInClassAsync(int classId, int reportPeriodId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = await unitOfWork.ClassesReportPeriodsRepository.IsExistReportPeriodInClassAsync(classId, reportPeriodId);
                return isExist;
            }
        }

        public async Task<bool> IsExistReportPeriodInClassAsync(List<ClassesReportPeriodDto> classReportPeriodsDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = false;
                foreach (var classReportPeriodDto in classReportPeriodsDto)
                {
                    isExist = await unitOfWork.ClassesReportPeriodsRepository
                        .IsExistReportPeriodInClassAsync(
                            classReportPeriodDto.ClassId, classReportPeriodDto.ReportPeriodId);
                    if (isExist)
                    {
                        return isExist;
                    }
                }
                
                return isExist;
            }
        }

        public async Task<OperationResult> DeleteClassReportPeroidAsync(int classReportPeriodId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classReportPeriod =
                    await unitOfWork.ClassesReportPeriodsRepository.GetByIdIncludingAllAsync(classReportPeriodId);
                if (classReportPeriod != null && classReportPeriod.ClassDatas.Count == 0)
                {
                    unitOfWork.ClassesReportPeriodsRepository.Remove(classReportPeriod);
                    await unitOfWork.CompleteAsync();

                    return new OperationResult(true, "Отчетный период успешно удален с класса");
                }

                return new OperationResult(false, "Невозможно удалить отчетный период с класса. Отчетный период содержит записи");
            }
        }

        public async Task<ClassesReportPeriods> GetClassReportPeriodByIdAsync(int classReportPeriodId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classReportPeriod =
                    await unitOfWork.ClassesReportPeriodsRepository.GetByIdIncludingAllAsync(classReportPeriodId);

                return classReportPeriod;
            }
        }

        public async Task<List<ReportPeriodDto>> GetUnassignedReportPeriodsByClassIdAsync(int classId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classesReportPeriods =
                    await unitOfWork.ClassesReportPeriodsRepository.GetByClassIdIncludingAll(classId);


                var reportPeriods = _mapper.Map<List<ReportPeriod>>(await _reportPeriodService.GetAllReportPeriodsAsync(new ReportPeriodFilterDto())) ;

                var unassignedReportPeriods =
                    _reportPeriodService.ExceptRepeatedReportPeriods(
                        classesReportPeriods.Select(x => x.ReportPeriod).ToList(), reportPeriods);

                return _mapper.Map<List<ReportPeriodDto>>(unassignedReportPeriods);
            }
        }

        public List<ClassesReportPeriodDto> ConvertToClassesReportPeriodDto(AddReportPeriodsToClassDto model)
        {
            List<ClassesReportPeriodDto> classesReportPeriods = new List<ClassesReportPeriodDto>();
            foreach (var reportPeriodId in model.ReportPeriodIds)
            {
                classesReportPeriods.Add(new ClassesReportPeriodDto()
                {
                    ReportPeriodId = reportPeriodId,
                    ClassId = model.ClassId
                });
            }

            return classesReportPeriods;
        }
    }
}
