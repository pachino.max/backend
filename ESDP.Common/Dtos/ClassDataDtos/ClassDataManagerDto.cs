﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.ClassDataDtos
{
    public class ClassDataManagerDto : BaseEntityDto
    {
        public string Teacher { get; set; }
        public string Subject { get; set; }
    }
}
