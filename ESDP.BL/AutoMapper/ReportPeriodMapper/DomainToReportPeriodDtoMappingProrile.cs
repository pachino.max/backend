﻿using AutoMapper;
using ESDP.Common.Dtos.ReportPeriodDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.ReportPeriodMapper
{
    public class DomainToReportPeriodDtoMappingProrile : Profile
    {
        public DomainToReportPeriodDtoMappingProrile()
        {
            CreateReportPeriodMap();
        }

        public void CreateReportPeriodMap()
        {
            CreateMap<ReportPeriod, ReportPeriodDto>();
        }
    }
}
