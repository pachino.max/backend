﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Common.Dtos.ClassDataDtos
{
    public class ClassDataDto : BaseEntityDto
    {
        [Required]
        public int ClassesReportPeriodsId { get; set; }
        [Required]
        public int AcademicSubjectId { get; set; }
        [Required]
        public string[] TeachersId { get; set; }
    }
}
