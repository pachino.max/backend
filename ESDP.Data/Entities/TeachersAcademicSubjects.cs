﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class TeachersAcademicSubjects : Entity
    {
        public int? AcademicSubjectsId { get; set; }
        public AcademicSubject Subjects { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }
    }
}
