﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Common.Dtos.IdentifierClassDtos
{
    public class CreateIdentifierClassDto : BaseEntityDto
    {
        [Required]
        public int Parallel { get; set; }
        [Required]
        public string Letter { get; set; }
    }
}
