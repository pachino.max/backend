﻿using System;
using System.Collections.Generic;
using System.Text;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Common.Dtos.IdentifierClassDtos;
using ESDP.Common.Dtos.ReportPeriodDtos;
using ESDP.Common.Dtos.TeachersDtos;

namespace ESDP.Common.Dtos.AcademicClassDtos
{
    public class AddClassDto
    {
        public List<IdentifierClassDto> Identifiers { get; set; }
        public List<TeacherManagerDto> Teachers { get; set; }
        public List<AcademicSubjectDto> AcademicSubjects { get; set; }
        public List<ReportPeriodDto> ReportPeriods { get; set; }
        public int? YearId { get; set; }
    }
}
