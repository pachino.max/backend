﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using AutoMapper;
using ESDP.BL;
using ESDP.Common.Dtos.UserDtos;
using ESDP.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ESDP.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserService _userService;

        private readonly IMapper _mapper;

        public AuthController(
            UserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Authorizes the user in the system.
        /// </summary>
        /// <remarks>
        /// Authorizes the user in the system and generates the JWT-token and returns its roles.
        /// Example of sending a request:
        /// 
        ///     POST /api/login
        ///     {
        ///         "email": "youremail@example.com",
        ///         "password": "yourPassword"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Subject.</returns>
        /// <response code="200">Returns code 200 and authorizes the user.</response>
        /// <response code="403">Returns code 403 if the user is blocked.</response>
        /// <response code="401">Returns code 401 if the user is not authorized.</response>
        /// <response code="400">Returns code 400 if the data sent is incorrect.</response>
        [Route("login")]
        [HttpPost]
        public async Task<ActionResult> Login([FromBody] LoginDto model)
        {
            
            if (ModelState.IsValid)
            {
                var user = await _userService.FindByEmailAsync(model.Email);
                if (user != null && await _userService.CheckPasswordAsync(user, model.Password))
                {
                    if (!user.IsBlocked)
                    {
                        
                        var token = await _userService.GetToken(user);
                        var roles =  await _userService.GetRolesAsync(user);
                        var userIdentity = _mapper.Map<UserIdentityDto>(user);

                        return Ok(new AuthorizeUserDto()
                        {
                            Token = new JwtSecurityTokenHandler().WriteToken(token),
                            Roles = roles,
                            UserIdentity = userIdentity
                        });
                        
                    }
                    return StatusCode(403);
                }
                return Unauthorized();
            }
            return BadRequest("Invalid Login model");
        }
    }
}
