﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class AddedEntityClassesReportPeriodsChangedFromClassDataPeriodIdToClassesReportPeriodsId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_ReportPeriods_PeriodId",
                table: "ClassDatas");

            migrationBuilder.RenameColumn(
                name: "PeriodId",
                table: "ClassDatas",
                newName: "ClassesReportPeriodsId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassDatas_PeriodId",
                table: "ClassDatas",
                newName: "IX_ClassDatas_ClassesReportPeriodsId");

            migrationBuilder.CreateTable(
                name: "ClassesReportPeriods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ClassId = table.Column<int>(nullable: false),
                    ReportPeriodId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassesReportPeriods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassesReportPeriods_Classes_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassesReportPeriods_ReportPeriods_ReportPeriodId",
                        column: x => x.ReportPeriodId,
                        principalTable: "ReportPeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClassesReportPeriods_ClassId",
                table: "ClassesReportPeriods",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassesReportPeriods_ReportPeriodId",
                table: "ClassesReportPeriods",
                column: "ReportPeriodId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas",
                column: "ClassesReportPeriodsId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas");

            migrationBuilder.DropTable(
                name: "ClassesReportPeriods");

            migrationBuilder.RenameColumn(
                name: "ClassesReportPeriodsId",
                table: "ClassDatas",
                newName: "PeriodId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassDatas_ClassesReportPeriodsId",
                table: "ClassDatas",
                newName: "IX_ClassDatas_PeriodId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_ReportPeriods_PeriodId",
                table: "ClassDatas",
                column: "PeriodId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
