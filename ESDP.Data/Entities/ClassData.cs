﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class ClassData : Entity
    {
        public int ClassesReportPeriodsId { get; set; }
        public ClassesReportPeriods ClassesReportPeriods { get; set; }

        public string TeacherId { get; set; }
        public User Teacher { get; set; }

        public int SubjectId { get; set; }
        public AcademicSubject Subject { get; set; }

        public Grades Grades { get; set; }
    }
}
