﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ESDP.Common.Dtos.TeachersAcademicSubjectsDto;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.AcademicSubjectToTeachersAcademicSubjectsMapper
{
    public class AcademicSubjectToSubjectWithTeachersMappingProfile : Profile
    {
        public AcademicSubjectToSubjectWithTeachersMappingProfile()
        {
            CreateMapping();
        }

        public void CreateMapping()
        {
            CreateMap<AcademicSubject, SubjectWithTeachersDto>()
                .ForMember(dest => dest.SubjectId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.SubjectName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Teachers, opt => opt.Ignore());
        }
    }
}
