﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class ClassesReportPeriods : Entity
    {
        public int ClassId { get; set; }

        public AcademicClass Class { get; set; }

        public int ReportPeriodId { get; set; }

        public ReportPeriod ReportPeriod { get; set; }

        public IList<ClassData> ClassDatas { get; set; }
    }
}
