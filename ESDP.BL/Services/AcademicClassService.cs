﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos;
using ESDP.Common.Dtos.AcademicClassDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Common.Dtos.ClassDataDtos;
using ESDP.Common.Dtos.ClassesReportPeriodsDtos;
using ESDP.Common.Dtos.IdentifierClassDtos;
using ESDP.Common.Dtos.ReportPeriodDtos;
using ESDP.Common.Dtos.TeachersDtos;
using ESDP.Common.Dtos.UserDtos;

namespace ESDP.BL.Services
{
    public class AcademicClassService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;
        private readonly ClassDataService _classDataService;
        private readonly TeachersService _teachersService;
        private readonly IdentifierClassService _identifierService;
        private readonly AcademicSubjectService _academicSubjectService;
        private readonly ReportPeriodService _reportPeriodService;
        private readonly AcademicYearService _academicYearService;
        private readonly ClassesReportPeriodsService _classesReportPeriodsService;

        public AcademicClassService(
            IUnitOfWorkFactory unitOfWorkFactory, 
            IMapper mapper, ClassDataService classDataService, 
            AcademicYearService academicYearService, 
            TeachersService teachersService, 
            IdentifierClassService identifierService, 
            AcademicSubjectService academicSubjectService, 
            ReportPeriodService reportPeriodService, 
            ClassesReportPeriodsService classesReportPeriodsService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
            _classDataService = classDataService;
            _academicYearService = academicYearService;
            _teachersService = teachersService;
            _identifierService = identifierService;
            _academicSubjectService = academicSubjectService;
            _reportPeriodService = reportPeriodService;
            _classesReportPeriodsService = classesReportPeriodsService;
        }

        public async Task<OperationResult> CreateAsync(AcademicClassDto model)
        {
            var academicClass = _mapper.Map<AcademicClassDto, AcademicClass>(model);
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = await IsExistAcademicClassWithThisYearAndIdentifierAsync(academicClass);
                if (isExist)
                {
                    return new OperationResult(false, $"Уже существует класс в данном учебном периоде");
                }
                await unitOfWork.AcademicClassRepository.CreateAsync(academicClass);
                await unitOfWork.CompleteAsync();
                return new OperationResult(true, "Класс успешно создан");
            }
        }

        private async Task<bool> IsExistAcademicClassWithThisYearAndIdentifierAsync(AcademicClass inputAcademicClass)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var existAcademicClass = await unitOfWork
                    .AcademicClassRepository.GetByIdentifierAndYearIdAsync(inputAcademicClass.IdentifierClassId, 
                        inputAcademicClass.YearId);
                return (existAcademicClass != null);
            }
        }

        public async Task<AddClassDto> GetClassCollectionsAsync()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                return new AddClassDto
                {
                    Teachers = _mapper.Map<List<TeacherManagerDto>>(await _teachersService.GetAllTeachersAsync()),
                    Identifiers = await _identifierService.GetAllIdentifierClass(new IdentifierClassFilterDto()),
                    YearId = await _academicYearService.GetIsCurrentYearIdAsync()
                };
            }
        }

        public async Task<OperationResult> DeleteAcademicClassAsync(int classId)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var academicClass = await unitOfWork.AcademicClassRepository.GetByIdIncludingAsync(classId);
                if (academicClass!= null && academicClass.ClassesReportPeriods.Count == 0)
                {
                    unitOfWork.AcademicClassRepository.Remove(academicClass);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Класс удален");
                }

                return new OperationResult(false, "Удаление класса невозможно, по нему есть отчетные периоды.");
            }
        }

        public async Task<List<AcademicClassManagerDto>> GetAcademicClassesAsync(AcademicClassFilterDto filter)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var academicClasses = await unitOfWork.AcademicClassRepository.GetAllIncludingAsync();
                var academicClassesViewModels = _mapper.Map<List<AcademicClassManagerDto>>(academicClasses);
                if (filter != null)
                {
                    if (filter.Fullname != null)
                    {
                        academicClassesViewModels = academicClassesViewModels.Where(x => x.ClassroomTeacherFullname.ToLower().Contains(filter.Fullname.ToLower())).ToList();
                    }
                    if (filter.SelectedIdentifier != null)
                    {
                        academicClassesViewModels = academicClassesViewModels.Where(x => x.Identifier.Contains(filter.SelectedIdentifier)).ToList();
                    }
                    if (filter.SelectedYear != null)
                    {
                        academicClassesViewModels = academicClassesViewModels.Where(x => x.YearName.Contains(filter.SelectedYear)).ToList();
                    }
                }
                return academicClassesViewModels;
            }           
        }

        public async Task<List<AcademicClassPeriodManager>> GetClassReportPeriodsAsync(int classId)
        {
            var classReportPeriods = await _classesReportPeriodsService.GetClassReportPeriodsByClassIdAsync(classId);
            var classReportPeriodsDto = _mapper.Map<List<AcademicClassPeriodManager>>(classReportPeriods);

            return classReportPeriodsDto;
        }

        public async Task<OperationResult> AddReportPeriodToClassAsync(AddReportPeriodsToClassDto model)
        {
            var classReportPeriodsDto = _classesReportPeriodsService.ConvertToClassesReportPeriodDto(model);


            var isExist = 
                await _classesReportPeriodsService.IsExistReportPeriodInClassAsync(classReportPeriodsDto);

            if (!isExist)
            {
                var classesReportPeriods = _mapper.Map<List<ClassesReportPeriods>>(classReportPeriodsDto);
                using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
                {
                    await unitOfWork.ClassesReportPeriodsRepository.CreateRangeAsync(classesReportPeriods);
                    await unitOfWork.CompleteAsync();

                    return new OperationResult(true, "Добавление в класс прошло успешно");
                }
            }

            return new OperationResult(false, "Отчетные периоды уже существуют");
        }

        public async Task<OperationResult> AddClassDataToClassAsync(ClassDataDto model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classData = _classDataService.ConvertToClassDatas(model);
                var isExist = await _classDataService.IsExistClassDataInClassAsync(classData);
                if (!isExist)
                {
                    await unitOfWork.ClassDataRepository.CreateRangeAsync(classData);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Данные успешно добавлены");
                }

                return new OperationResult(true, "Такие данные уже существуют");
            }
        }

        public async Task<OperationResult> DeleteReportPeriodFromClass(int classReportPeriod)
        {
            var result = await _classesReportPeriodsService.DeleteClassReportPeroidAsync(classReportPeriod);
            return result;
        }

        public async Task<AcademicClassReportPeriodManagerDto> GetAcademicClassByIdAsync(int classId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var academicClass = await unitOfWork.AcademicClassRepository.GetByIdIncludingAsync(classId);
                var academicClassDto = _mapper.Map<AcademicClassReportPeriodManagerDto>(academicClass);
                academicClassDto.ReportPeriodDtos = new List<ReportPeriodDto>();
                var classesReportPeriods =
                        await _classesReportPeriodsService.GetClassReportPeriodsByClassIdAsync(classId);
                foreach (var period in classesReportPeriods)
                {
                    academicClassDto.ReportPeriodDtos.Add(_mapper.Map<ReportPeriodDto>(period.ReportPeriod));
                }
                return academicClassDto;
            }

        }

        public async Task<AcademicClassPeriodManager> GetClassReportPeriodByIdAsync(int classReportPeriodId)
        {
            var classReportPeriod =
                await _classesReportPeriodsService.GetClassReportPeriodByIdAsync(classReportPeriodId);
            var classReportPeriodDto = _mapper.Map<AcademicClassPeriodManager>(classReportPeriod);

            return classReportPeriodDto;
        }

        public async Task<OperationResult> DeleteClassDataAsync(int classesDataId)
        {
            var result = await _classDataService.DeleteClassDataAsync(classesDataId);
            return result;
        }
    } 
}

