﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class TeachersAcademicSubjectsConfiguration : IEntityTypeConfiguration<TeachersAcademicSubjects>
    {
        public void Configure(EntityTypeBuilder<TeachersAcademicSubjects> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Subjects).WithMany(x => x.Teachers).HasForeignKey(x => x.AcademicSubjectsId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);
            builder.HasOne(x => x.User).WithMany(x => x.Subjects).HasForeignKey(x => x.UserId).IsRequired(true).OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
