﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class ClassDataEntityChangedReportPeriodToClassesReportPeriods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_ClassesReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas",
                column: "ClassesReportPeriodsId",
                principalTable: "ClassesReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_ClassesReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas",
                column: "ClassesReportPeriodsId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
