﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class ClassDataConfiguration : IEntityTypeConfiguration<ClassData>
    {
        public void Configure(EntityTypeBuilder<ClassData> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Teacher).WithMany(x => x.ClassDatas).HasForeignKey(x => x.TeacherId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict).IsRequired(true);
            builder.HasOne(x => x.Subject).WithMany(x => x.ClassDatas).HasForeignKey(x => x.SubjectId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.ClassesReportPeriods).WithMany(x => x.ClassDatas).HasForeignKey(x => x.ClassesReportPeriodsId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
