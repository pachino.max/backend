﻿using AutoMapper;
using ESDP.Common.Dtos.ClassDataDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.ClassDataMapper
{
    public class DomainToClassDataDtoMappingProfile : Profile
    {
        public DomainToClassDataDtoMappingProfile()
        {
            CreateClassDataMap();
        }

        public void CreateClassDataMap()
        {
            CreateMap<ClassData, ClassDataManagerDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Teacher, opts => opts.MapFrom(vm => vm.Teacher.Surname + " " + vm.Teacher.MiddleName + " " + vm.Teacher.Name))
                .ForMember(src => src.Subject, opts => opts.MapFrom(vm => vm.Subject.Name));
        }
    }
}