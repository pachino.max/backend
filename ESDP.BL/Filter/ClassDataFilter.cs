﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESDP.Common.Dtos.TeacherPageDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.Filter
{
    public static class ClassDataFilter
    {
        public static List<ClassData> Filter(this List<ClassData> data, TeacherPageFilterDto filter)
        {
            if (filter?.ReportPeriod != null)
            {
                data = data.Where(x =>
                        x.ClassesReportPeriods.ReportPeriod.Name.ToLower().Contains(filter.ReportPeriod.ToLower()))
                    .ToList();
            }

            if (filter?.Subject != null)
            {
                data = data.Where(x => x.Subject.Name.ToLower().Contains(filter.Subject.ToLower())).ToList();
            }

            if (filter?.CurrentYear == true)
            {
                data = data.Where(x => x.ClassesReportPeriods.Class.Year.IsCurrent).ToList();
            }

            return data;
        }
    }
}
