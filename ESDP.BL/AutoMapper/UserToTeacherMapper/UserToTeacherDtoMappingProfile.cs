﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ESDP.Common.Dtos.TeachersDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.UserToTeacherMapper
{
    public class UserToTeacherDtoMappingProfile : Profile
    {
        public UserToTeacherDtoMappingProfile()
        {
            CreateToTeacherDtoMapping();
        }

        private void CreateToTeacherDtoMapping()
        {
            CreateMap<User, TeacherDto>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Fullname,
                opt => opt.MapFrom(src => src.Surname + " " + src.Name + " " + src.MiddleName));

        }
    }
}
