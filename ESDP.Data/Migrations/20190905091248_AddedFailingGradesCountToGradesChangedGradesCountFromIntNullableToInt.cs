﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class AddedFailingGradesCountToGradesChangedGradesCountFromIntNullableToInt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GradesId",
                table: "ClassDatas");

            migrationBuilder.AlterColumn<int>(
                name: "TwoCount",
                table: "Grades",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ThreeCount",
                table: "Grades",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FourCount",
                table: "Grades",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FiveCount",
                table: "Grades",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FailingGradesCount",
                table: "Grades",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FailingGradesCount",
                table: "Grades");

            migrationBuilder.AlterColumn<int>(
                name: "TwoCount",
                table: "Grades",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ThreeCount",
                table: "Grades",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FourCount",
                table: "Grades",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FiveCount",
                table: "Grades",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "GradesId",
                table: "ClassDatas",
                nullable: true);
        }
    }
}
