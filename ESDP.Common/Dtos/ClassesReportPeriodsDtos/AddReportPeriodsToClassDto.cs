﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.ClassesReportPeriodsDtos
{
    public class AddReportPeriodsToClassDto
    {
        public int ClassId { get; set; }

        public int[] ReportPeriodIds { get; set; }
    }
}
