﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class AddOnoToOneGradesClassDataAndOnToManyAcademicYearAcademicClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_AcademicYears_YearId",
                table: "ClassDatas");

            migrationBuilder.DropIndex(
                name: "IX_Grades_ClassDataId",
                table: "Grades");

            migrationBuilder.DropIndex(
                name: "IX_ClassDatas_YearId",
                table: "ClassDatas");

            migrationBuilder.DropColumn(
                name: "YearId",
                table: "ClassDatas");

            migrationBuilder.AddColumn<int>(
                name: "YearId",
                table: "Classes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GradesId",
                table: "ClassDatas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Grades_ClassDataId",
                table: "Grades",
                column: "ClassDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Classes_YearId",
                table: "Classes",
                column: "YearId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_AcademicYears_YearId",
                table: "Classes",
                column: "YearId",
                principalTable: "AcademicYears",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_AcademicYears_YearId",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_Grades_ClassDataId",
                table: "Grades");

            migrationBuilder.DropIndex(
                name: "IX_Classes_YearId",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "YearId",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "GradesId",
                table: "ClassDatas");

            migrationBuilder.AddColumn<int>(
                name: "YearId",
                table: "ClassDatas",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Grades_ClassDataId",
                table: "Grades",
                column: "ClassDataId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDatas_YearId",
                table: "ClassDatas",
                column: "YearId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_AcademicYears_YearId",
                table: "ClassDatas",
                column: "YearId",
                principalTable: "AcademicYears",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
