﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.AcademicClassDtos;
using ESDP.Common.Dtos.ClassDataDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ESDP.Common;
using ESDP.Common.Dtos.ClassesReportPeriodsDtos;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class AcademicClassController : Controller
    {
        private readonly AcademicClassService _academicClassService;
        private readonly ClassDataService _classDataService;
        private readonly ClassesReportPeriodsService _classesReportPeriodsService;

        public AcademicClassController(AcademicClassService academicClassService, ClassDataService classDataService, ClassesReportPeriodsService classesReportPeriodsService)
        {
            _academicClassService = academicClassService;
            _classDataService = classDataService;
            _classesReportPeriodsService = classesReportPeriodsService;
        }

        /// <summary>
        /// Create new AcademicClass
        /// </summary>
        /// <remarks>
        /// Creates a new class based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/addAcademicClass
        ///     {
        ///         "yearId": 1,
        ///         "classroomTeacherId": "cc33539f-7b3f-4b8f-929b-54ec9a1619cf",
        ///         "identifierClassId": 1,
        ///         "classDatas": [{"periodId": 1, "academicSubjectId": 1, "teacherId": "cc33539f-7b3f-4b8f-929b-54ec9a1619cf"},
        ///         {"periodId": 1, "academicSubjectId": 2, "teacherId": "cc33539f-7b3f-4b8f-929b-54ec9a1619cf"}]
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Class created successfully</returns>
        /// <response code="200">Returns code 200 and a message about the creation of the class.</response>
        /// <response code="500">Returns code 500 if the model is invalid.</response>
        [Route("api/addAcademicClass")]
        [HttpPost]
        public async Task<IActionResult> CreateAcademicClass([FromBody] AcademicClassDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicClassService.CreateAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/getAcademicClassById/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetAcademicClassById(int id)
        {
            var academicClassDto = await _academicClassService.GetAcademicClassByIdAsync(id);
            return Ok(academicClassDto);
        }

        /// <summary>
        /// Returns a list of classes
        /// </summary>
        /// <remarks>
        /// Returns a list of classes, either all, or according to the filter.
        /// The filter works in any register and by incomplete content.
        /// Example request with filtering by class identifier name and school year name:
        /// 
        ///     GET /api/academicClassManager
        ///     {
        ///         "fullname": null,
        ///         "selectedIdentifier": "5А",
        ///         "selectedYear": "2019-2020 учебный год"
        ///     }
        ///     
        /// </remarks>
        /// <param name="filter"></param>
        /// <returns>All academic classes</returns>
        /// <response code="200">Returns code 200 and a list of classes.</response>
        [Route("api/getAllAcademicClasses")]
        [HttpGet]
        public async Task<IActionResult> GetAllAcademicClasses(AcademicClassFilterDto filter)
        {
            var academicClassViewModels = await _academicClassService.GetAcademicClassesAsync(filter);
            return Ok(academicClassViewModels);
        }

        /// <summary>
        /// Deletes the selected class by id.
        /// </summary>
        /// <remarks>
        /// Deletes the selected class by id.
        /// Example of sending a request:
        /// 
        ///     POST /api/deleteAcademicClass
        ///     {
        ///         "id": 1
        ///     }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Class deleted successfully.</returns>
        /// <response code="200">Returns code 200 and a success message.</response>
        /// <response code="500">It returns code 500 and a message stating that deletion is not possible, because the class has grades or code 500 if id is 0</response>
        [Route("api/deleteAcademicClass")]
        [HttpPost]
        public async Task<IActionResult> DeleteAcademicClass(int id)
        {
            var result = await _academicClassService.DeleteAcademicClassAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/getClassCollections")]
        [HttpGet]
        public async Task<IActionResult> GetClassCollection()
        {
            return Ok(await _academicClassService.GetClassCollectionsAsync());
        }

        [Route("api/deleteReportPeriodFromClass")]
        [HttpPost]
        public async Task<ActionResult> DeleteReportPeriodFromClass([FromQuery] int id)
        {
            var result = await _academicClassService.DeleteReportPeriodFromClass(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }

        [Route("api/getUnAssignedReportPeriods/{classId}")]
        [HttpGet]
        public async Task<ActionResult> GetUnassignedReportPeriodsFromClassesReportPeriods(int classId)
        {
            var reportPeriodsDto = await _classesReportPeriodsService.GetUnassignedReportPeriodsByClassIdAsync(classId);
            return Ok(reportPeriodsDto);
        }

        [Route("api/addReportPeriodToClass")]
        [HttpPost]
        public async Task<IActionResult> AddReportPeriodToClass([FromBody] AddReportPeriodsToClassDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicClassService.AddReportPeriodToClassAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }

            return StatusCode(500, new OperationResult(false, "Введены неверный данные"));
        }

        [Route("api/getClassReportPeriods/{classId}")]
        [HttpGet]
        public async Task<ActionResult> GetClassReportPeriods(int classId)
        {
            var classReportPeriods = await _academicClassService.GetClassReportPeriodsAsync(classId);
            return Ok(classReportPeriods);
        }

        [Route("api/getClassReportPeriod/{classReportPeriodId}")]
        [HttpGet]
        public async Task<ActionResult> GetClassReportPeriodById(int classReportPeriodId)
        {
            var classReportPeriod = await _academicClassService.GetClassReportPeriodByIdAsync(classReportPeriodId);
            return Ok(classReportPeriod);
        }

        [Route("api/getClassReportPeriodClassDatas/{classReportPeriodId}")]
        [HttpGet]
        public async Task<ActionResult> GetClassReportPeriodClassDatas(int classReportPeriodId)
        {
            var classDataModel = await _classDataService.GetClassReportPeriodClassDatasAsync(classReportPeriodId);
            return Ok(classDataModel);
        }

        [Route("api/addClassDataToClass")]
        [HttpPost]
        public async Task<ActionResult> AddClassDataToClass([FromBody] ClassDataDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicClassService.AddClassDataToClassAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }

            return StatusCode(500, new OperationResult(false, "Введены неверный данные"));
        }

        [Route("api/deleteClassData")]
        [HttpPost]
        public async Task<ActionResult> DeleteClassData(int id)
        {
            var result = await _academicClassService.DeleteClassDataAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }
    }
}
