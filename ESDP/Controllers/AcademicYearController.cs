﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.AcademicYearDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class AcademicYearController : Controller
    {
        private readonly AcademicYearService _academicYearService;

        public AcademicYearController(AcademicYearService service)
        {
            _academicYearService = service;
        }

        /// <summary>
        /// Create new AcademicYear
        /// </summary>
        /// <remarks>
        /// Creates a new year based on the Dto-model, not all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/createAcademicYear
        ///     {
        ///         "name": "2019-2020 учебный год",
	    ///         "begindate": "2019-09-01",
	    ///         "enddate": "2020-05-25",
	     ///        "isCurrent": "true"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Year created.</returns>
        /// <response code="200">Returns code 200 and a message about the creation of the year.</response>
        /// <response code="500">Returns code 500 if the model is not valid or code 500 and a message stating that such an year already exists.</response>
        [Route("api/createAcademicYear")]
        [HttpPost]
        public async Task<IActionResult> CreateAcademicYear([FromBody] AcademicYearDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicYearService.CreateAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Edits the selected school year.
        /// </summary>
        /// <remarks>
        /// Edits the selected school year based on the Dto-model, not all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/editAcademicYear
        ///     {
        ///         "id": 1,
        ///         "name": "2018-2019 учебный год",
        ///         "begindate": "2018-09-01",
        ///         "enddate": "2019-05-25",
        ///         "isCurrent": "false"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Year created.</returns>
        /// <response code="200">Returns code 200 and message about saving changes.</response>
        /// <response code="500">Returns code 500 if the model is not valid or code 500 and a message stating that such an year already exists.</response>
        [Route("api/editAcademicYear")]
        [HttpPost]
        public async Task<IActionResult> EditAcademicYear([FromBody] AcademicYearDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicYearService.EditAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Deletes the selected school year.
        /// </summary>
        /// <remarks>
        /// Deletes the selected school year by its id.
        /// Example of sending a request:
        /// 
        ///     POST /api/deleteAcademicYear
        ///     {
        ///         "id": 1
        ///     }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Year created.</returns>
        /// <response code="200">Returns code 200 and the message about the removal of the school year.</response>
        /// <response code="500">Returns code 500 if the sent id is not correct or code 500 and a message stating that the school year cannot be deleted.</response>
        [Route("api/deleteAcademicYear")]
        [HttpPost]
        public async Task<IActionResult> DeleteAcademicYear(int id)
        {
            var result = await _academicYearService.DeleteAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }

        /// <summary>
        /// Returns all school years.
        /// </summary>
        /// <remarks>
        /// Returns all school years, or filtered by name.
        /// The filter works case-insensitive and for incomplete content.
        /// Example of sending a request:
        /// 
        ///     GET /api/academicYearManager
        ///     {
        ///         "name": "2018-2019"
        ///     }
        ///     
        /// </remarks>
        /// <param name="filterDto"></param>
        /// <returns>Years.</returns>
        /// <response code="200">Returns code 200 and a list of school years found.</response>
        [Route("api/academicYearManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllAcademicYears(AcademicYearFilterDto filterDto)
        {
            var academicYearsViewModels = await _academicYearService.GetAllAcademicYears(filterDto);
            return Ok(academicYearsViewModels);
        }

        /// <summary>
        /// Returns the year.
        /// </summary>
        /// <remarks>
        /// Returns the school year by its id.
        /// Example of sending a request:
        /// 
        ///     GET /api/academicYear/1
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Subject.</returns>
        /// <response code="200">Returns code 200 and the school year found by id.</response>
        /// <response code="500">Returns code 500 if the sent id is incorrect.</response>
        [Route("api/academicYear/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetYearAsync(int id)
        {
            return Ok(await _academicYearService.GetByIdAsync(id));
        }
    }
}
