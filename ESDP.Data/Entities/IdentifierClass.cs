﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class IdentifierClass : Entity
    {
        public string Letter { get; set; }
        public int Parallel { get; set; }

        public IList<AcademicClass> AcademicClasses { get; set; }
    }
}
