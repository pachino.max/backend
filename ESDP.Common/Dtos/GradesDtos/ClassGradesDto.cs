﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.GradesDtos
{
    public class ClassGradesDto
    {
        public string ClassTeacher { get; set; }
        public string ReportPeriod { get; set; }
        public string Subject { get; set; }
        public string IdentifierClass { get; set; }
        public GradesDto Grades { get; set; }

        public int ClassDataId { get; set; }
    }
}
