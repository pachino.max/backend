﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos;
using ESDP.Common.Dtos.ClassDataDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.BL.Services
{
    public class ClassDataService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public ClassDataService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public async Task<List<ClassDataManagerDto>> GetClassReportPeriodClassDatasAsync(int classReportPeriodId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classDatas =
                    await unitOfWork.ClassDataRepository.GetByClassesReportPeriodsIdIncludingAllAsync(classReportPeriodId);

                var classDatasDto = _mapper.Map<List<ClassDataManagerDto>>(classDatas);

                return classDatasDto;
            }
        }

        public async Task<bool> IsExistClassDataInClassAsync(ClassData classData)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = await unitOfWork.ClassDataRepository.IsExistClassDataInClassAsync(classData);
                return isExist;
            }
        }

        public async Task<bool> IsExistClassDataInClassAsync(List<ClassData> classData)
        {
            var isExist = false;
            foreach (var clData in classData)
            {
                isExist = await IsExistClassDataInClassAsync(clData);
                if (isExist)
                {
                    return isExist;
                }
            }

            return isExist;
        }

        public List<ClassData> ConvertToClassDatas(ClassDataDto model)
        {
            var classDatas = new List<ClassData>();

            foreach (var teacherId in model.TeachersId)
            {
                classDatas.Add(new ClassData()
                {
                    ClassesReportPeriodsId = model.ClassesReportPeriodsId,
                    SubjectId = model.AcademicSubjectId,
                    TeacherId = teacherId
                });
            }

            return classDatas;
        }

        public async Task<OperationResult> DeleteClassDataAsync(int classesDataId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classData = await unitOfWork.ClassDataRepository.GetByIdIncludingGradesAndSubjectAsync(classesDataId);
                if (classData?.Grades != null)
                {
                    return new OperationResult(false, "Класс дата не может быть удалена, есть оценки");
                }

                unitOfWork.ClassDataRepository.Remove(classData);
                await unitOfWork.CompleteAsync();

                return new OperationResult(true, "Класс дата успешно удалена");
            }
        }

        public async Task<bool> isExistTeacherWithSubjectAsync(int subjectId, string userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = await unitOfWork.ClassDataRepository.IsExistTeacherSubjectAsync(subjectId, userId);
                return isExist;
            }
        }

        public async Task<List<ClassData>> GetClassDataByTeacherIdAsync(string teacherId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classDatas = await unitOfWork.ClassDataRepository.GetByTeacherIdIncludingSubjectAcademicClassReportPeriodAsync(teacherId);
                return classDatas;
            }
        }
    }
}
