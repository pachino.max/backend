﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace ESDP.Data.Repositories.ClassesReportPeriods
{
    public class ClassesReportPeriodsRepository : Repository<Entities.ClassesReportPeriods>, IClassesReportPeriodsRepository
    {
        public ClassesReportPeriodsRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<Entities.ClassesReportPeriods>> GetByClassIdIncludingAll(int classId)
        {
            return await _context.ClassesReportPeriods
                .Include(x => x.Class)
                .Include(x => x.ReportPeriod)
                .Where(x => x.ClassId == classId)
                .ToListAsync();
        }

        public async Task<bool> IsExistReportPeriodInClassAsync(int classId, int reportPeriodId)
        {
            return await _context.ClassesReportPeriods
                .AnyAsync(x => x.ClassId == classId && x.ReportPeriodId == reportPeriodId);
        }

        public async Task<bool> IsExistReportPeriodInClassAsync(int reportPeriodId)
        {
            return await _context.ClassesReportPeriods
                .AnyAsync(x =>  x.ReportPeriodId == reportPeriodId);
        }

        public async Task<Entities.ClassesReportPeriods> GetByIdIncludingAllAsync(int classReportPeriodId)
        {
            return await _context.ClassesReportPeriods
                .Include(x => x.ClassDatas)
                .Include(x=>x.ReportPeriod)
                .Include(x=>x.Class)
                .FirstOrDefaultAsync(x => x.Id == classReportPeriodId);
        }

        public async Task CreateRangeAsync(List<Entities.ClassesReportPeriods> classesReportPeriods)
        {
            await _context.AddRangeAsync(classesReportPeriods);
        }
    }

}
