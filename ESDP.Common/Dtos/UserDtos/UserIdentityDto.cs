﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.UserDtos
{
    public class UserIdentityDto
    {
        public string UserId { get; set; }

        public string FullName { get; set; }
    }
}
