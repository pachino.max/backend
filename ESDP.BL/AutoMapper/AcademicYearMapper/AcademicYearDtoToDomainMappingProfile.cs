﻿using AutoMapper;
using ESDP.Common.Dtos.AcademicYearDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.AcademicYearMapper
{
    public class AcademicYearDtoToDomainMappingProfile : Profile
    {
        public AcademicYearDtoToDomainMappingProfile()
        {
            CreateAcademicYearDtoMap();
        }

        public void CreateAcademicYearDtoMap()
        {
            CreateMap<AcademicYearDto, AcademicYear>();
        }
    }
}
