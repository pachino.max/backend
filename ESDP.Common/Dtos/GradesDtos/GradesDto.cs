﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Common.Dtos.GradesDtos
{
    public class GradesDto
    {
        [Range(0, int.MaxValue)]
        public int FiveCount { get; set; }

        [Range(0, int.MaxValue)]
        public int FourCount { get; set; }

        [Range(0, int.MaxValue)]
        public int ThreeCount { get; set; }

        [Range(0, int.MaxValue)]
        public int TwoCount { get; set; }

        [Range(0, int.MaxValue)]
        public int FailingGradesCount { get; set; }

        public int ClassDataId { get; set; }
    }
}
