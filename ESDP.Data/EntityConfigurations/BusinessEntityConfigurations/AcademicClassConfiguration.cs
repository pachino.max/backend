﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class AcademicClassConfiguration : IEntityTypeConfiguration<AcademicClass>
    {
        public void Configure(EntityTypeBuilder<AcademicClass> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Identifier).WithMany(x => x.AcademicClasses).HasForeignKey(x => x.IdentifierClassId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.ClassroomTeacher).WithMany(x => x.Classes).HasForeignKey(x => x.ClassroomTeracherId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Year).WithMany(x => x.Classes).HasForeignKey(x => x.YearId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasMany(x => x.ClassesReportPeriods).WithOne(x => x.Class).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
