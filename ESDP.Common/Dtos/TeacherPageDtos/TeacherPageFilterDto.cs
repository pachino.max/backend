﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.TeacherPageDtos
{
    public class TeacherPageFilterDto
    {
        public string ReportPeriod { get; set; }

        public string Subject { get; set; }

        public bool? CurrentYear { get; set; }
    }
}
