﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESDP.Data.Context;
using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace ESDP.Data.Repositories.Grade
{
    public class GradeRepository : Repository<Entities.Grades>, IGradeRepository
    {
        public GradeRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<bool> IsExistByClassDataIdAsync(int classDataId)
        {
            return await _context.Grades.AnyAsync(x => x.ClassDataId == classDataId);
        }

        public Task<Grades> GetByClassDataIdAsync(int classDataId)
        {
            return _context.Grades.FirstOrDefaultAsync(x => x.ClassDataId == classDataId);
        }
    }
}
