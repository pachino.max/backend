﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class RemovedOneToOneRelationshipInIdentifierClassToAcademicClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IdentifierClasses_Classes_AcademicClassId",
                table: "IdentifierClasses");

            migrationBuilder.DropIndex(
                name: "IX_IdentifierClasses_AcademicClassId",
                table: "IdentifierClasses");

            migrationBuilder.DropColumn(
                name: "AcademicClassId",
                table: "IdentifierClasses");

            migrationBuilder.CreateIndex(
                name: "IX_Classes_IdentifierClassId",
                table: "Classes",
                column: "IdentifierClassId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_IdentifierClasses_IdentifierClassId",
                table: "Classes",
                column: "IdentifierClassId",
                principalTable: "IdentifierClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_IdentifierClasses_IdentifierClassId",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_Classes_IdentifierClassId",
                table: "Classes");

            migrationBuilder.AddColumn<int>(
                name: "AcademicClassId",
                table: "IdentifierClasses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_IdentifierClasses_AcademicClassId",
                table: "IdentifierClasses",
                column: "AcademicClassId",
                unique: true,
                filter: "[AcademicClassId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_IdentifierClasses_Classes_AcademicClassId",
                table: "IdentifierClasses",
                column: "AcademicClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
