﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class AcademicSubjectController : Controller
    {
        private readonly AcademicSubjectService _academicSubjectService;

        public AcademicSubjectController(AcademicSubjectService academicSubjectService)
        {
            _academicSubjectService = academicSubjectService;
        }

        /// <summary>
        /// Create new AcademicSubject
        /// </summary>
        /// <remarks>
        /// Creates a new subject based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/addAcademicSubject
        ///     {
        ///         "name": "Математика"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Subject created.</returns>
        /// <response code="200">Returns code 200 and a message about the creation of the subject.</response>
        /// <response code="500">Returns code 500 if the model is not valid or code 500 and a message stating that such an item already exists.</response>
        [Route("api/addAcademicSubject")]
        [HttpPost]
        public async Task<IActionResult> CreateAcademicSubject([FromBody] AcademicSubjectDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicSubjectService.CreateAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Edits a selected subject
        /// </summary>
        /// <remarks>
        /// Edits the selected subject based on the Dto model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/editAcademicSubject
        ///     {
        ///         "id": 1
        ///         "name": "Алгебра"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Subject modified.</returns>
        /// <response code="200">Returns code 200 and a message stating that the subject has been modified.</response>
        /// <response code="500">Returns code 500 if the model is not valid or code 500 and a message stating that such an item already exists.</response>
        [Route("api/editAcademicSubject")]
        [HttpPost]
        public async Task<IActionResult> EditAcademicSubject([FromBody] AcademicSubjectDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicSubjectService.EditAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Deletes the selected subject
        /// </summary>
        /// <remarks>
        /// Deletes the selected subject by its id.
        /// Example of sending a request:
        /// 
        ///     POST /api/deleteAcademicSubject
        ///     {
        ///         "id": 1
        ///     }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Subject deleted.</returns>
        /// <response code="200">Returns code 200 and a message stating that the subject has been deleted.</response>
        /// <response code="500">Returns code 500 if the sent id is incorrect or code 500 and a message stating that deletion is not possible.</response>
        [Route("api/deleteAcademicSubject")]
        [HttpPost]
        public async Task<IActionResult> DeleteAcademicSubject(int id)
        {
            var result = await _academicSubjectService.DeleteAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }

        /// <summary>
        /// Returns the subject.
        /// </summary>
        /// <remarks>
        /// Returns the subject by its id.
        /// Example of sending a request:
        /// 
        ///     GET /api/academicSubject/1
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Subject.</returns>
        /// <response code="200">Returns code 200 and the subject found by id.</response>
        /// <response code="500">Returns code 500 if the sent id is incorrect.</response>
        [Route("api/academicSubject/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetSubjectAsync(int id)
        {
            return Ok(await _academicSubjectService.GetByIdAsync(id));
        }

        /// <summary>
        /// Returns all school subjects.
        /// </summary>
        /// <remarks>
        /// Returns all academic subjects, or subjects filtered by name.
        /// The filter works case-insensitive and for incomplete content.
        /// Example of sending a request:
        /// 
        ///     GET /api/academicSubjectManager
        ///     {
        ///         "name": "Математика"
        ///     }
        ///     
        /// </remarks>
        /// <param name="filterDto"></param>
        /// <returns>Subjects.</returns>
        /// <response code="200">Returns code 200 and all found educational subjects.</response>
        [Route("api/academicSubjectManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllAcademicSubjects(AcademicSubjectFilterDto filterDto)
        {
            var academicSubjectViewModels = await _academicSubjectService.GetAllAcademicSubjectAsync(filterDto);
            return Ok(academicSubjectViewModels);
        }

        [Route("api/getSubjectWithTeachers/{subjectId}")]
        [HttpGet]
        public async Task<ActionResult> GetSubjectWithTeachers(int subjectId)
        {
            var subject = await _academicSubjectService.GetSubjectWithTeachersAsync(subjectId);
            return Ok(subject);
        }
    }
}
