﻿using AutoMapper;
using ESDP.Common.Dtos.TeachersDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.TeacherMapper
{
    public class TeacherDtoToDomainMappingProfile : Profile
    {
        public TeacherDtoToDomainMappingProfile()
        {
            CreateTeacherDtoMap();
        }

        public void CreateTeacherDtoMap()
        {
            CreateMap<TeacherDto, TeachersAcademicSubjects>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.UserId, opts => opts.MapFrom(vm => vm.UserId))
                .ForMember(src => src.AcademicSubjectsId, opts => opts.Ignore());
        }
    }
}
