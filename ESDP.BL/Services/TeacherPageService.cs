﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ESDP.BL.Filter;
using ESDP.Common;
using ESDP.Common.Dtos.GradesDtos;
using ESDP.Common.Dtos.TeacherPageDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using Microsoft.AspNetCore.Identity;

namespace ESDP.BL.Services
{
    public class TeacherPageService
    {
        private readonly IMapper _mapper;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        private readonly UserService _userService;

        private readonly ClassDataService _classDataService;

        private readonly GradesService _gradesService;

        private readonly AcademicYearService _academicYearService;

        public TeacherPageService(
            IMapper mapper, 
            IUnitOfWorkFactory unitOfWorkFactory, 
            UserService userService, 
            ClassDataService classDataService, GradesService gradesService, AcademicYearService academicYearService)
        {
            _mapper = mapper;
            _unitOfWorkFactory = unitOfWorkFactory;
            _userService = userService;
            _classDataService = classDataService;
            _gradesService = gradesService;
            _academicYearService = academicYearService;
        }

        public async Task<TeacherPageManagerDto> GetTeachersClassTeachingAsync(
            string userName, TeacherPageFilterDto filter)
        {

            var currentUser = await _userService.FindUserByUserNameAsync(userName);
            var classDatas = await _classDataService.GetClassDataByTeacherIdAsync(currentUser.Id);
            filter.CurrentYear = true;
            classDatas = classDatas.Filter(filter);
            var teacherPageClassDataDtos = _mapper.Map<List<TeacherPageClassDataDto>>(classDatas);
            var teacherPageManagerDto = new TeacherPageManagerDto(currentUser.Id, teacherPageClassDataDtos);

            return teacherPageManagerDto;

        }

        public async Task<ClassGradesDto> GetClassGradesAsync(int classDataId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var classData = await unitOfWork.ClassDataRepository.GetByIdIncludingGradesAndSubjectAsync(classDataId);
                var classReportPeriod =
                    await unitOfWork.ClassesReportPeriodsRepository.GetByIdIncludingAllAsync(
                        classData.ClassesReportPeriodsId);
                var academicClass =
                    await unitOfWork.AcademicClassRepository.GetByIdIncludingAsync(classReportPeriod.ClassId);
                var grades = classData.Grades ?? new Grades();

                var classGradesDto = new ClassGradesDto()
                {
                    Subject = classData.Subject.Name,
                    ClassTeacher = $"{academicClass.ClassroomTeacher.Name} {academicClass.ClassroomTeacher.MiddleName} {academicClass.ClassroomTeacher.Surname}",
                    IdentifierClass = $"{academicClass.Identifier.Parallel}{academicClass.Identifier.Letter}",
                    ReportPeriod = classReportPeriod.ReportPeriod.Name,
                    ClassDataId = classData.Id,
                    Grades = _mapper.Map<GradesDto>(grades)
                };

                return classGradesDto;
            }
        }

        public async Task<OperationResult> AddGradesAsync(GradesDto model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = await unitOfWork.GradeRepository.IsExistByClassDataIdAsync(model.ClassDataId);
                if (isExist)
                {
                    return await _gradesService.ChangeGradesAsync(model);
                }
                else
                {
                    return await _gradesService.AddNewGradesAsync(model);
                }
            }
        }
    }
}
