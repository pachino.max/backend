﻿using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.ReportPeriod
{
    public class ReportPeriodRepository : Repository<Entities.ReportPeriod>, IReportPeriodRepository
    {
        public ReportPeriodRepository(ApplicationDbContext context) : base(context)
        {

        }

    }
}
