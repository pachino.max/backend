﻿using System;
using System.Text;

namespace ESDP.Common.Dtos.AcademicClassDtos
{
    public class AcademicClassManagerDto : BaseEntityDto
    {
        public string Identifier { get; set; }
        public string ClassroomTeacherFullname { get; set; }
        public string YearName { get; set; }
    }

}
