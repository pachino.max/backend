﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.AcademicYearDtos
{
    public class AcademicYearFilterDto
    {
        public string Name { get; set; }
    }
}
