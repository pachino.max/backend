﻿using AutoMapper;
using ESDP.Common.Dtos.AcademicClassDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.AcademicClassMapper
{
    public class AcademicClassToAcademicClassReportPeriodDtoMappingProfile : Profile
    {
        public AcademicClassToAcademicClassReportPeriodDtoMappingProfile()
        {
            CreateAcademicClassMap();
        }

        public void CreateAcademicClassMap()
        {
            CreateMap<AcademicClass, AcademicClassReportPeriodManagerDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Identifier,
                    opts => opts.MapFrom(vm => vm.Identifier.Parallel.ToString() + vm.Identifier.Letter))
                .ForMember(src => src.ClassroomTeacherFullname,
                    opts => opts.MapFrom(vm =>
                        vm.ClassroomTeacher.Surname + " " + vm.ClassroomTeacher.Name + " " +
                        vm.ClassroomTeacher.MiddleName))
                .ForMember(src => src.YearName, opts => opts.MapFrom(vm => vm.Year.Name));
        }
    }
}