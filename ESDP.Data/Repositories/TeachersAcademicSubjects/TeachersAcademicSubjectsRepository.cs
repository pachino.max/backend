﻿using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.TeachersAcademicSubjects
{
    public class TeachersAcademicSubjectsRepository : Repository<Entities.TeachersAcademicSubjects>, ITeachersAcademicSubjectsRepository
    {
        public TeachersAcademicSubjectsRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<Entities.TeachersAcademicSubjects> GetByIdIncludingAsync(int id)
        {
            return await _context.TeachersAcademicSubjects
                .Include(x => x.Subjects)
                .Include(x => x.User)
                .SingleOrDefaultAsync(x => x.Id == id);
        }
        public async Task<IEnumerable<Entities.TeachersAcademicSubjects>> GetByTeacherIdIncludingAsync(string teacherId)
        {
            return await _context.TeachersAcademicSubjects
                .Include(x => x.Subjects)
                .Include(x => x.User)
                .Where(x => x.UserId == teacherId).ToListAsync();
        }

        public async Task<IEnumerable<Entities.TeachersAcademicSubjects>> GetAllIncludingAsync()
        {
            return await _context.TeachersAcademicSubjects
                .Include(x => x.Subjects)
                .Include(x => x.User).ToListAsync();
        }

        public async Task CreateRangeAsync(List<Entities.TeachersAcademicSubjects> teachersAcademicSubjects)
        {
            await _context.AddRangeAsync(teachersAcademicSubjects);
        }
        public void RemoveRange(params Entities.TeachersAcademicSubjects[] teachersAcademicSubjects)
        {
            _context.RemoveRange(teachersAcademicSubjects);
        }

        public void UpdateRange(params Entities.TeachersAcademicSubjects[] teachersAcademicSubjects)
        {
            _context.UpdateRange(teachersAcademicSubjects);
        }

        public async Task<bool> IsExistSubjectsAtTeacherAsync(string teacherId, int? teacherAcademicSubjectsId)
        {
            return await _context.TeachersAcademicSubjects
                .AnyAsync(x => x.UserId == teacherId && x.AcademicSubjectsId == teacherAcademicSubjectsId.Value);
        }

        public async Task<List<Entities.TeachersAcademicSubjects>> GetBySubjectIdIncludingAll(int subjectId)
        {
            return await _context.TeachersAcademicSubjects
                .Include(x => x.User)
                .Where(x => x.AcademicSubjectsId == subjectId)
                .ToListAsync();
        }
    }
}
