﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ESDP.Common.Dtos.GradesDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.GradesMapper
{
    public class DomainToGradesDtoMappingProfile : Profile
    {
        public DomainToGradesDtoMappingProfile()
        {
            CreateGradesDtoMapping();
        }

        private void CreateGradesDtoMapping()
        {
            CreateMap<Grades, GradesDto>();
        }
    }
}
