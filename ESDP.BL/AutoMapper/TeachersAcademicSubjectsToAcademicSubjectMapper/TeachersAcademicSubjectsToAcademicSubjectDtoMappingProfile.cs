﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.TeachersAcademicSubjectsToAcademicSubjectMapper
{
    public class TeachersAcademicSubjectsToAcademicSubjectDtoMappingProfile : Profile
    {
        public TeachersAcademicSubjectsToAcademicSubjectDtoMappingProfile()
        {
            CreateMappingProfile();
        }

        private void CreateMappingProfile()
        {
            CreateMap<TeachersAcademicSubjects, AcademicSubjectDto>()
                .ForMember(dest => dest.TeacherAcademicSubjectId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Subjects.Name))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AcademicSubjectsId));
        }
    }
}
