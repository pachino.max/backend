﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Common.Dtos.ClassesReportPeriodsDtos
{
    public class ClassesReportPeriodDto
    {
        [Required] public int ClassId { get; set; }
        [Required] public int ReportPeriodId { get; set; }
    }
}
