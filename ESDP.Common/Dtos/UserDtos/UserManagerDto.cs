﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.UserDtos
{
    public class UserManagerDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public IList<string> RolesName { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }
    }
}
