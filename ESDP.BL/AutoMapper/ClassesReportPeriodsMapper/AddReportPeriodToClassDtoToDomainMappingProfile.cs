﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ESDP.Common.Dtos.ClassesReportPeriodsDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.ClassesReportPeriodsMapper
{
    public class AddReportPeriodToClassDtoToDomainMappingProfile : Profile
    {
        public AddReportPeriodToClassDtoToDomainMappingProfile()
        {
            CreateClassesReportPeriods();
        }

        public void CreateClassesReportPeriods()
        {
            CreateMap<ClassesReportPeriodDto, ClassesReportPeriods>()
                .ForMember(src => src.ClassId, opt => opt.MapFrom(vr => vr.ClassId))
                .ForMember(src => src.ReportPeriodId, opt => opt.MapFrom(vr => vr.ReportPeriodId));


        }
    }
}
