﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Context
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateContext();
    }
}
