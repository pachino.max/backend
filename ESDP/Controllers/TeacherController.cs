﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.TeachersDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher, Teacher")]
    public class TeacherController : Controller
    {
        private readonly TeachersService _teacherService;

        public TeacherController(TeachersService service)
        {
            _teacherService = service;
        }

        /// <summary> 
        /// </summary>
        /// <remarks>
        /// Edits the selected teacher based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     GET /api/editTeacher/1
        ///     
        /// </remarks>
        /// <param name="id"></param>
        [Authorize(Roles = "Administrator, Director, Headteacher, Teacher")]
        [Route("api/getTeacherById/{teacherId}")]
        [HttpGet]
        public async Task<ActionResult> GetTeacherSubjectsByTeacherId(string teacherId)
        {
            if (ModelState.IsValid)
            {
                var teacher = await _teacherService.GetTeacherWithSubjectsAsync(teacherId);

                return Ok(teacher);
            }
            return StatusCode(400);
        }

        /// <summary>
        /// Edits the selected teacher.
        /// </summary>
        /// <remarks>
        /// Edits the selected teacher based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/editTeacher
        ///     {
        ///         "userId": "cc33539f-7b3f-4b8f-929b-54ec9a1619cf",
        ///         "academicSubjectsId": [3,4,6]
        ///     }
        ///     
        /// </remarks>
        /// <param name="teacherDto"></param>
        /// <returns>Teacher modified.</returns>
        /// <response code="200">Returns code 200 and a message stating that the changes have been saved.</response>
        /// <response code="500">Returns code 500 if the model is invalid or code 500 and a message stating that such a teacher already exists.</response>
        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/addSubjectsToTeacher")]
        [HttpPost]
        public async Task<IActionResult> AddSubjectsToTeacher([FromBody] TeacherDto teacherDto)
        {
            if (ModelState.IsValid)
            {
                var result = await _teacherService.AddSubjectsToTeacherAsync(teacherDto);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Returns all teachers.
        /// </summary>
        /// <remarks>
        /// Returns all teachers, or filtered by fullname and their subjects.
        /// The filter works case-insensitive and for incomplete content.
        /// Example of sending a request:
        /// 
        ///     GET /api/teacherManager
        ///     {
        ///         "fullname": "Иван Петров",
        ///         "selectedSubject": "Информатика"
        ///     }
        ///     
        /// </remarks>
        /// <param name="filterDto"></param>
        /// <returns>Teachers.</returns>
        /// <response code="200">Returns code 200 and a list of teachers found.</response>
        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/teacherManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllTeachers(TeacherFilterDto filterDto)
        {
            var teacherViewModels = await _teacherService.GetAllTeachersWithSubjectsAsync(filterDto);
            return Ok(teacherViewModels);
        }

        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/deleteSubjectFromTeacher/")]
        [HttpPost]
        public async Task<ActionResult> DeleteSubjectFromTeacher(int id)
        {
            var result = await _teacherService.DeleteSubjectFromTeacherAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }

        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/getUnassignedSubjectsFromTeacher/{teacherId}")]
        [HttpGet]
        public async Task<ActionResult> GetUnassignedSubjectsFromTeacher(string teacherId)
        {
            var subjects = await _teacherService.GetUnassignedSubjectsAsync(teacherId);
            return Ok(subjects);
        }
    }
}
