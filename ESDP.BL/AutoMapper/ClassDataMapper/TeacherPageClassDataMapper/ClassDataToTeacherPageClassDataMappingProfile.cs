﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ESDP.Common.Dtos.TeacherPageDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.ClassDataMapper.TeacherPageClassDataMapper
{
    public class ClassDataToTeacherPageClassDataMappingProfile : Profile
    {
        public ClassDataToTeacherPageClassDataMappingProfile()
        {
            CreateTeacherPageMapp();
        }

        private void CreateTeacherPageMapp()
        {
            CreateMap<ClassData, TeacherPageClassDataDto>()
                .ForMember(dest => dest.ClassDataId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.IdentifierClass, opt => opt.MapFrom(
                    src => $"{src.ClassesReportPeriods.Class.Identifier.Parallel}{src.ClassesReportPeriods.Class.Identifier.Letter}"))
                .ForMember(dest => dest.SubjectName, opt => opt.MapFrom(src => src.Subject.Name));
        }
    }
}
