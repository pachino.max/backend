﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class GradesConfiguration : IEntityTypeConfiguration<Grades>
    {
        public void Configure(EntityTypeBuilder<Grades> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.ClassData).WithOne(x => x.Grades).HasForeignKey<Grades>(x => x.ClassDataId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
