﻿using AutoMapper;
using ESDP.Common.Dtos.AcademicClassDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.AcademicClassMapper
{
    public class AcademicClassDtoToDomainMappingProfile : Profile
    {
        public AcademicClassDtoToDomainMappingProfile()
        {
            CreateAcademicClassDtoMap();
        }

        public void CreateAcademicClassDtoMap()
        {
            CreateMap<AcademicClassDto, AcademicClass>()
                .ForMember(src => src.IdentifierClassId, opts => opts.MapFrom(vm => vm.IdentifierClassId))
                .ForMember(src => src.ClassroomTeracherId, opts => opts.MapFrom(vm => vm.ClassroomTeacherId))
                .ForMember(src => src.YearId, opts => opts.MapFrom(vm => vm.YearId));
        }
    }
}
