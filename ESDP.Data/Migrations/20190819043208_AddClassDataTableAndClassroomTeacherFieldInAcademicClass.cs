﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class AddClassDataTableAndClassroomTeacherFieldInAcademicClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Grades_Classes_ClassId",
                table: "Grades");

            migrationBuilder.DropForeignKey(
                name: "FK_Grades_ReportPeriods_PeriodId",
                table: "Grades");

            migrationBuilder.DropForeignKey(
                name: "FK_Grades_AcademicSubjects_SubjectId",
                table: "Grades");

            migrationBuilder.DropForeignKey(
                name: "FK_Grades_AspNetUsers_TeacherId",
                table: "Grades");

            migrationBuilder.DropForeignKey(
                name: "FK_Grades_AcademicYears_YearId",
                table: "Grades");

            migrationBuilder.DropIndex(
                name: "IX_Grades_ClassId",
                table: "Grades");

            migrationBuilder.DropIndex(
                name: "IX_Grades_PeriodId",
                table: "Grades");

            migrationBuilder.DropIndex(
                name: "IX_Grades_SubjectId",
                table: "Grades");

            migrationBuilder.DropIndex(
                name: "IX_Grades_TeacherId",
                table: "Grades");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "Grades");

            migrationBuilder.DropColumn(
                name: "PeriodId",
                table: "Grades");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Grades");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Grades");

            migrationBuilder.RenameColumn(
                name: "YearId",
                table: "Grades",
                newName: "ClassDataId");

            migrationBuilder.RenameIndex(
                name: "IX_Grades_YearId",
                table: "Grades",
                newName: "IX_Grades_ClassDataId");

            migrationBuilder.AddColumn<string>(
                name: "ClassroomTeracherId",
                table: "Classes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ClassDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ClassId = table.Column<int>(nullable: false),
                    YearId = table.Column<int>(nullable: false),
                    PeriodId = table.Column<int>(nullable: false),
                    TeacherId = table.Column<string>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassDatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassDatas_Classes_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassDatas_ReportPeriods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "ReportPeriods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassDatas_AcademicSubjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "AcademicSubjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassDatas_AspNetUsers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassDatas_AcademicYears_YearId",
                        column: x => x.YearId,
                        principalTable: "AcademicYears",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Classes_ClassroomTeracherId",
                table: "Classes",
                column: "ClassroomTeracherId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDatas_ClassId",
                table: "ClassDatas",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDatas_PeriodId",
                table: "ClassDatas",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDatas_SubjectId",
                table: "ClassDatas",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDatas_TeacherId",
                table: "ClassDatas",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassDatas_YearId",
                table: "ClassDatas",
                column: "YearId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_AspNetUsers_ClassroomTeracherId",
                table: "Classes",
                column: "ClassroomTeracherId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grades_ClassDatas_ClassDataId",
                table: "Grades",
                column: "ClassDataId",
                principalTable: "ClassDatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_AspNetUsers_ClassroomTeracherId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Grades_ClassDatas_ClassDataId",
                table: "Grades");

            migrationBuilder.DropTable(
                name: "ClassDatas");

            migrationBuilder.DropIndex(
                name: "IX_Classes_ClassroomTeracherId",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "ClassroomTeracherId",
                table: "Classes");

            migrationBuilder.RenameColumn(
                name: "ClassDataId",
                table: "Grades",
                newName: "YearId");

            migrationBuilder.RenameIndex(
                name: "IX_Grades_ClassDataId",
                table: "Grades",
                newName: "IX_Grades_YearId");

            migrationBuilder.AddColumn<int>(
                name: "ClassId",
                table: "Grades",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PeriodId",
                table: "Grades",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId",
                table: "Grades",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TeacherId",
                table: "Grades",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Grades_ClassId",
                table: "Grades",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_Grades_PeriodId",
                table: "Grades",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_Grades_SubjectId",
                table: "Grades",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Grades_TeacherId",
                table: "Grades",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Grades_Classes_ClassId",
                table: "Grades",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grades_ReportPeriods_PeriodId",
                table: "Grades",
                column: "PeriodId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grades_AcademicSubjects_SubjectId",
                table: "Grades",
                column: "SubjectId",
                principalTable: "AcademicSubjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grades_AspNetUsers_TeacherId",
                table: "Grades",
                column: "TeacherId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grades_AcademicYears_YearId",
                table: "Grades",
                column: "YearId",
                principalTable: "AcademicYears",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
