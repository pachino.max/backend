﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.AcademicSubjectDtos
{
    public class AcademicSubjectFilterDto
    {
        public string Name { get; set; }
    }
}
