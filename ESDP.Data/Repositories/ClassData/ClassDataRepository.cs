﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace ESDP.Data.Repositories.ClassData
{
    public class ClassDataRepository : Repository<Entities.ClassData>, IClassDataRepository
    {
        public ClassDataRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<Entities.ClassData>> GetByClassesReportPeriodsIdIncludingAllAsync(int classReportPeriodId)
        {
            return await _context.ClassDatas
                .Include(x => x.Teacher)
                .Include(x => x.Subject)
                .Include(x => x.Grades)
                .Where(x => x.ClassesReportPeriodsId == classReportPeriodId)
                .ToListAsync();
        }

        public async Task<bool> IsExistClassDataInClassAsync(Entities.ClassData classData)
        {
            return await _context.ClassDatas.AnyAsync(x =>
                x.ClassesReportPeriodsId == classData.ClassesReportPeriodsId && x.SubjectId == classData.SubjectId &&
                x.TeacherId == classData.TeacherId);
        }

        public async Task CreateRangeAsync(List<Entities.ClassData> classData)
        {
            await _context.AddRangeAsync(classData);
        }

        public async Task<Entities.ClassData> GetByIdIncludingGradesAndSubjectAsync(int classesDataId)
        {
            return await _context.ClassDatas
                .Include(x=>x.Grades)
                .Include(x=>x.Subject)
                .FirstOrDefaultAsync(x => x.Id == classesDataId);
        }

        public async Task<bool> IsExistTeacherSubjectAsync(int subjectId, string userId)
        {
            return await _context.ClassDatas.AnyAsync(x => x.SubjectId == subjectId && x.TeacherId == userId);
        }

        public async Task<List<Entities.ClassData>> GetByTeacherIdIncludingSubjectAcademicClassReportPeriodAsync(string teacherId)
        {
            return await _context.ClassDatas
                .Include(x => x.Subject)
                .Include(x=>x.ClassesReportPeriods.Class.Identifier)
                .Include(x=>x.ClassesReportPeriods.ReportPeriod)
                .Include(x=>x.ClassesReportPeriods.Class.Year)
                .Where(x => x.TeacherId == teacherId)
                .ToListAsync();
        }
    }
}
