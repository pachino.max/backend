﻿using AutoMapper;
using ESDP.Common.Dtos.TeachersDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.TeacherMapper
{
    public class DomainToTeacherDtoMappingProfile : Profile
    {
        public DomainToTeacherDtoMappingProfile()
        {
            CreateTeacherMap();
        }

        public void CreateTeacherMap()
        {
            CreateMap<TeachersAcademicSubjects, TeacherManagerDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.UserId))
                .ForMember(src => src.Fullname, opts => opts.MapFrom(vm => vm.User.Surname + " " + vm.User.Name + " " + vm.User.MiddleName))
                .ForMember(src => src.SubjectsName, opts => opts.Ignore());
        }
    }
}
