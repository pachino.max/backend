﻿using AutoMapper;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.AcademicSubjectMapper
{
    class AcademicSubjectDtoToDomainMappingProfile : Profile
    {
        public AcademicSubjectDtoToDomainMappingProfile()
        {
            CreateAcademicSubjectDtoMap();
        }

        public void CreateAcademicSubjectDtoMap()
        {
            CreateMap<AcademicSubjectDto, AcademicSubject>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name));
        }
    }
}
