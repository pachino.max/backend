﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.TeachersAcademicSubjects
{
    public interface ITeachersAcademicSubjectsRepository : IRepository<Entities.TeachersAcademicSubjects>
    {
        Task<Entities.TeachersAcademicSubjects> GetByIdIncludingAsync(int id);
        Task<IEnumerable<Entities.TeachersAcademicSubjects>> GetByTeacherIdIncludingAsync(string teacherId);
        Task<IEnumerable<Entities.TeachersAcademicSubjects>> GetAllIncludingAsync();
        Task CreateRangeAsync(List<Entities.TeachersAcademicSubjects> teachersAcademicSubjects);
        void RemoveRange(params Entities.TeachersAcademicSubjects[] teachersAcademicSubjects);
        void UpdateRange(params Entities.TeachersAcademicSubjects[] teachersAcademicSubjects);
        Task<bool> IsExistSubjectsAtTeacherAsync(string teacherId, int? teacherAcademicSubjectsId);
        Task<List<Entities.TeachersAcademicSubjects>> GetBySubjectIdIncludingAll(int subjectId);
    }
}
