﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Data.Entities
{
    public class Grades : Entity
    {
        [Range(0, int.MaxValue)]
        public int FiveCount { get; set; }

        [Range(0, int.MaxValue)]
        public int FourCount { get; set; }

        [Range(0, int.MaxValue)]
        public int ThreeCount { get; set; }

        [Range(0, int.MaxValue)]
        public int TwoCount { get; set; }

        [Range(0, int.MaxValue)]
        public int FailingGradesCount { get; set; }

        public int ClassDataId { get; set; }
        public ClassData ClassData { get; set; }
    }
}
