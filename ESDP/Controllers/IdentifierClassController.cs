﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.IdentifierClassDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class IdentifierClassController : Controller
    {
        private readonly IdentifierClassService _identifierClassService;
        public IdentifierClassController(IdentifierClassService service)
        {
            _identifierClassService = service;
        }

        /// <summary>
        /// Create new IdentifierClass
        /// </summary>
        /// <remarks>
        /// Creates a new classroom identifier based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/addIdentifierClass
        ///     {
        ///         "parallel": 5,
        ///         "letter": "Б"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Year created.</returns>
        /// <response code="200">Returns code 200 and the message about the creation of the identifier of the classroom.</response>
        /// <response code="500">Returns code 500 if the model is invalid or code 500 and a message stating that such a class identifier already exists.</response>
        [Route("api/addIdentifierClass")]
        [HttpPost]
        public async Task<IActionResult> CreateIdentifierClass([FromBody] CreateIdentifierClassDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _identifierClassService.CreateAsync(model);
                if (result.Result)
                {
                    return Ok(result);
                }
                return StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Edits the selected classroom identifier.
        /// </summary>
        /// <remarks>
        /// Edits the selected classroom identifier based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/editIdentifierClass
        ///     {
        ///         "id": 1,
        ///         "parallel": 5,
        ///         "letter": "А"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Year created.</returns>
        /// <response code="200">Returns code 200 and a message stating that the changes have been saved.</response>
        /// <response code="500">Returns code 500 if the model is invalid or code 500 and a message stating that such a class identifier already exists.</response>
        [Route("api/editIdentifierClass")]
        [HttpPost]
        public async Task<IActionResult> EditIdentifierClass([FromBody] CreateIdentifierClassDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _identifierClassService.EditAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Deletes the selected classroom identifier.
        /// </summary>
        /// <remarks>
        /// Deletes the selected classroom identifier by its id.
        /// Example of sending a request:
        /// 
        ///     POST /api/deleteIdentifierClass
        ///     {
        ///         "id": 1
        ///     }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Year created.</returns>
        /// <response code="200">Returns code 200 and the message about the removal of the classroom identifier.</response>
        /// <response code="500">Returns code 500 if the sent id is not correct or code 500 and a message stating that the classroom identifier cannot be deleted.</response>
        [Route("api/deleteIdentifierClass")]
        [HttpPost]
        public async Task<IActionResult> DeleteIdentifierClass(int id)
        {
            var result = await _identifierClassService.DeleteAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }

        /// <summary>
        /// Returns all classroom identifiers.
        /// </summary>
        /// <remarks>
        /// Returns all classroom identifiers, or filtered by name.
        /// The filter works case-insensitive and for incomplete content.
        /// Example of sending a request:
        /// 
        ///     GET /api/identifierClassManager
        ///     {
        ///         "identifier": "5Б"
        ///     }
        ///     
        /// </remarks>
        /// <param name="filterDto"></param>
        /// <returns>Years.</returns>
        /// <response code="200">Returns code 200 and a list of classroom identifiers found.</response>
        [Route("api/identifierClassManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllIdentifierClasses(IdentifierClassFilterDto filterDto)
        {
            var academicSubjectViewModels = await _identifierClassService.GetAllIdentifierClass(filterDto);
            return Ok(academicSubjectViewModels);
        }

        /// <summary>
        /// Returns the classroom identifier.
        /// </summary>
        /// <remarks>
        /// Returns the classroom identifier by its id.
        /// Example of sending a request:
        /// 
        ///     GET /api/identifierClass/1
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Subject.</returns>
        /// <response code="200">Returns code 200 and the classroom identifier found by id.</response>
        /// <response code="500">Returns code 500 if the sent id is incorrect.</response>
        [Route("api/identifierClass/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetIdentifierClass(int id)
        {
            return Ok(await _identifierClassService.FindByIdAsync(id));
        }
    }
}
