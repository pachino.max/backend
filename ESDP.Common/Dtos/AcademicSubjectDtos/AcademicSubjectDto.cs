﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ESDP.Common.Dtos.AcademicSubjectDtos
{
    public class AcademicSubjectDto : BaseEntityDto
    {
        public int TeacherAcademicSubjectId { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
