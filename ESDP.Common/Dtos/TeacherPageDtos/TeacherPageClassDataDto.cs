﻿namespace ESDP.Common.Dtos.TeacherPageDtos
{
    public class TeacherPageClassDataDto
    {
        public int ClassDataId { get; set; }

        public string IdentifierClass { get; set; }

        public string SubjectName { get; set; }
    }
}