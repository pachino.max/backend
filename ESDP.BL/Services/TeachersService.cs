﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Common.Dtos.TeachersDtos;
using ESDP.Common.Dtos.UserDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.BL.Services
{
    public class TeachersService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;
        private readonly AcademicSubjectService _subjectService;
        private readonly UserService _userService;
        private readonly UserManager<User> _userManager;
        private readonly ClassDataService _classDataService;

        public TeachersService(
            IUnitOfWorkFactory unitOfWorkFactory, 
            IMapper mapper, 
            AcademicSubjectService subjectService, 
            UserService userService, 
            UserManager<User> userManager, 
            ClassDataService classDataService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
            _subjectService = subjectService;
            _userService = userService;
            _userManager = userManager;
            _classDataService = classDataService;
        }

        public async Task<OperationResult> AddSubjectsToTeacherAsync(TeacherDto teacherDto)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teachers = ConvertToTeachersAcademicSubjects(teacherDto);

                var isExist = await IsExistSubjectsAtTeacherAsync(teachers);
                if (!isExist)
                {
                    await unitOfWork.TeachersAcademicSubjectsRepository.CreateRangeAsync(teachers);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Предметы добавлены");
                }

                return new OperationResult(true, "Предметы уже назначены");
            }
        }

        private async Task<bool> IsExistSubjectsAtTeacherAsync(List<TeachersAcademicSubjects> teachers)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = false;
                foreach (var teacher in teachers)
                {
                    isExist = await unitOfWork.TeachersAcademicSubjectsRepository
                        .IsExistSubjectsAtTeacherAsync(
                            teacher.UserId, teacher.AcademicSubjectsId);
                    if (isExist)
                    {
                        return isExist;
                    }
                }

                return isExist;
            }
        }

        private List<TeachersAcademicSubjects> ConvertToTeachersAcademicSubjects(TeacherDto teacherDto)
        {
            var teachers = new List<TeachersAcademicSubjects>();
            foreach (var subjectId in teacherDto.AcademicSubjectsId)
            {
                teachers.Add(new TeachersAcademicSubjects()
                {
                    UserId = teacherDto.UserId,
                    AcademicSubjectsId = subjectId
                });
            }

            return teachers;
        }

        public async Task<List<TeacherManagerDto>> GetAllTeachersWithSubjectsAsync(TeacherFilterDto filterDto)
        {
            var allTeachers = await GetAllTeachersAsync();

            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teachersWithSubject = await unitOfWork.TeachersAcademicSubjectsRepository.GetAllIncludingAsync();
                var allTeachersViewModel = _mapper.Map<List<TeacherManagerDto>>(allTeachers);
                allTeachersViewModel = AllTeachersSubjectsInitialize(teachersWithSubject, allTeachersViewModel);

                allTeachersViewModel = FilterTeachersBy(allTeachersViewModel, filterDto);

                return allTeachersViewModel;
            }
        }

        private List<TeacherManagerDto> FilterTeachersBy(List<TeacherManagerDto> allTeachersViewModel, TeacherFilterDto filterDto)
        {
            if (filterDto.Fullname != null && filterDto.SelectedSubject == null)
            {
                return allTeachersViewModel.Where(x => x.Fullname.ToLower().Contains(filterDto.Fullname.ToLower()) && x.IsDeleted == false).ToList();
            }
            if (filterDto.SelectedSubject != null && filterDto.Fullname == null)
            {
                return allTeachersViewModel.Where(x => x.SubjectsName.Contains(filterDto.SelectedSubject) && x.IsDeleted == false).ToList();
            }
            if (filterDto.Fullname != null && filterDto.SelectedSubject != null)
            {
                return allTeachersViewModel.Where(x => x.Fullname.ToLower().Contains(filterDto.Fullname.ToLower()) && x.SubjectsName.Contains(filterDto.SelectedSubject) && x.IsDeleted == false).ToList();
            }
            return allTeachersViewModel.Where(x => x.IsDeleted == false).ToList();
        }

        private List<TeacherManagerDto> AllTeachersSubjectsInitialize(IEnumerable<TeachersAcademicSubjects> teachersWithSubject, List<TeacherManagerDto> allTeachersViewModel)
        {
            foreach (var t in allTeachersViewModel)
            {
                t.SubjectsName = teachersWithSubject
                    .Where(x => x.UserId == t.Id).Select(x => x.Subjects.Name).ToList();
            }

            return allTeachersViewModel;
        }

        public async Task<List<AcademicSubjectDto>> GetAllSubject()
        {
            return await _subjectService.GetAllAcademicSubjectAsync();
        }

        public async Task<List<UserManagerDto>> GetAllTeachersAsync()
        {
            UserFilterDto filterDto = new UserFilterDto
            {
                SelectedRole = "Teacher"
            };
            return await _userService.GetAllUsers(filterDto);
        }

        public async Task<TeacherManagerDto> GetTeacherWithSubjectsAsync(string teacherId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teacher = _mapper.Map<UserManagerDto>(await _userManager.FindByIdAsync(teacherId));
                var teacherWithSubjects =
                    await unitOfWork.TeachersAcademicSubjectsRepository.GetByTeacherIdIncludingAsync(teacherId);
                var teacherViewModel = _mapper.Map<TeacherManagerDto>(teacher);

                teacherViewModel.AssignedSubjects =
                    _mapper.Map<List<AcademicSubjectDto>>(teacherWithSubjects);

                return teacherViewModel;
            }

        }

        public async Task<OperationResult> DeleteSubjectFromTeacherAsync(int teacherSubjectId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teacherSubject =
                    await unitOfWork.TeachersAcademicSubjectsRepository.GetByIdAsync(teacherSubjectId);

                var isExist =
                    await _classDataService.isExistTeacherWithSubjectAsync(teacherSubject.AcademicSubjectsId.Value,
                        teacherSubject.UserId);

                if (!isExist)
                {
                    unitOfWork.TeachersAcademicSubjectsRepository.Remove(teacherSubject);
                    await unitOfWork.CompleteAsync();

                    return new OperationResult(true, "Предмет успешно удален у преподавателя");
                }

                return new OperationResult(false, "Предмет не может быть удален, по нему есть записи");
            }
        }

        public async Task<List<AcademicSubjectDto>> GetUnassignedSubjectsAsync(string teacherId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teacherAcademicSubjects =
                    await unitOfWork.TeachersAcademicSubjectsRepository.GetByTeacherIdIncludingAsync(teacherId);

                var assignedSubjects = teacherAcademicSubjects.Select(x => x.Subjects).ToList();

                assignedSubjects = await _subjectService.ExceptRepeatedSubjects(assignedSubjects);

                var unAssignedSubjsDto = _mapper.Map<List<AcademicSubjectDto>>(assignedSubjects);

                return unAssignedSubjsDto;
            }
        }
    }
}
