﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.IdentifierClassDtos
{
    public class IdentifierClassFilterDto
    {
        public string Identifier { get; set; }
    }
}
