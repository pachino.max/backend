﻿using AutoMapper;
using ESDP.Common.Dtos.IdentifierClassDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.IdentifierClassMapper
{
    public class CreateIdentifierClassDtoToDomainMappingProfile : Profile
    {
        public CreateIdentifierClassDtoToDomainMappingProfile()
        {
            CreateCreateIdentifierClassDtoMap();
        }
        public void CreateCreateIdentifierClassDtoMap()
        {
            CreateMap<CreateIdentifierClassDto, IdentifierClass>();
        }
    }
}
