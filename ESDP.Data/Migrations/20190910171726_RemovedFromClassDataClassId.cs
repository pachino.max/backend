﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class RemovedFromClassDataClassId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_Classes_ClassId",
                table: "ClassDatas");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassesReportPeriods_Classes_ClassId",
                table: "ClassesReportPeriods");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassesReportPeriods_ReportPeriods_ReportPeriodId",
                table: "ClassesReportPeriods");

            migrationBuilder.DropIndex(
                name: "IX_ClassDatas_ClassId",
                table: "ClassDatas");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "ClassDatas");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas",
                column: "ClassesReportPeriodsId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassesReportPeriods_Classes_ClassId",
                table: "ClassesReportPeriods",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassesReportPeriods_ReportPeriods_ReportPeriodId",
                table: "ClassesReportPeriods",
                column: "ReportPeriodId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassesReportPeriods_Classes_ClassId",
                table: "ClassesReportPeriods");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassesReportPeriods_ReportPeriods_ReportPeriodId",
                table: "ClassesReportPeriods");

            migrationBuilder.AddColumn<int>(
                name: "ClassId",
                table: "ClassDatas",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ClassDatas_ClassId",
                table: "ClassDatas",
                column: "ClassId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_Classes_ClassId",
                table: "ClassDatas",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassDatas_ReportPeriods_ClassesReportPeriodsId",
                table: "ClassDatas",
                column: "ClassesReportPeriodsId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassesReportPeriods_Classes_ClassId",
                table: "ClassesReportPeriods",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassesReportPeriods_ReportPeriods_ReportPeriodId",
                table: "ClassesReportPeriods",
                column: "ReportPeriodId",
                principalTable: "ReportPeriods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
