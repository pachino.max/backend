﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.UnitOfWork
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}
