﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESDP.Common.Dtos.TeachersAcademicSubjectsDto;
using ESDP.Common.Dtos.TeachersDtos;

namespace ESDP.BL.Services
{
    public class AcademicSubjectService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public AcademicSubjectService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public async Task<OperationResult> CreateAsync(AcademicSubjectDto academicSubjectDto)
        {
            var academicSubject = _mapper.Map<AcademicSubjectDto, AcademicSubject>(academicSubjectDto);
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var checkAcademicSubjects = await unitOfWork.AcademicSubjectRepository.GetAllAsync();
                if(checkAcademicSubjects.FirstOrDefault(x => x.Name.ToLower() == academicSubject.Name.ToLower()) == null)
                {
                    await unitOfWork.AcademicSubjectRepository.CreateAsync(academicSubject);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Предмет создан");
                }
                return new OperationResult(false, "Предмет с таким именем уже существует");
            }
        }

        public async Task<OperationResult> EditAsync(AcademicSubjectDto academicSubjectDto)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var academicSubject = await unitOfWork.AcademicSubjectRepository.GetByIdAsync(academicSubjectDto.Id);
                var checkAcademicSubjects = await unitOfWork.AcademicSubjectRepository.GetAllAsync();
                if (academicSubject != null && checkAcademicSubjects.FirstOrDefault(c => c.Name.ToLower() == academicSubjectDto.Name.ToLower()) == null)
                {
                    academicSubject = _mapper.Map(academicSubjectDto, academicSubject);
                    unitOfWork.AcademicSubjectRepository.Update(academicSubject);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Предмет изменен");
                }
                return new OperationResult(false, "Предмет с таким именем уже существует");
            }
        }

        public async Task<OperationResult> DeleteAsync(int id)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var academicSubject = await unitOfWork.AcademicSubjectRepository.GetByIdIncludingAsync(id);
                if(academicSubject != null && academicSubject.Teachers.Count == 0)
                {
                    unitOfWork.AcademicSubjectRepository.Remove(academicSubject);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Предмет удален");
                }
                return new OperationResult(false, "Удаление невозможно. У данного предмета есть назначенные преподаватели");
            }
        }

        public async Task<List<AcademicSubjectDto>> GetAllAcademicSubjectAsync(AcademicSubjectFilterDto filterDto = null)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var academicSubjects = await unitOfWork.AcademicSubjectRepository.GetAllAsync();
                var academicSubjectViewModels = academicSubjects.Select(_mapper.Map<AcademicSubjectDto>).ToList();
                if(filterDto?.Name != null)
                {
                    return academicSubjectViewModels.Where(x => x.Name.ToLower().Contains(filterDto.Name.ToLower()) && x.IsDeleted == false).ToList();
                }
                return academicSubjectViewModels.Where(x => x.IsDeleted == false).ToList();
            }
        }
        public async Task<AcademicSubject> GetByIdAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                return await unitOfWork.AcademicSubjectRepository.GetByIdAsync(id);
            }
        }

        public async Task<SubjectWithTeachersDto> GetSubjectWithTeachersAsync(int subjectId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {                
                var teachersSubjects =
                    await unitOfWork.TeachersAcademicSubjectsRepository.GetBySubjectIdIncludingAll(subjectId);
                var subject = await unitOfWork.AcademicSubjectRepository.GetByIdAsync(subjectId);

                var subjectWithTeachers = _mapper.Map<SubjectWithTeachersDto>(subject);
                var users = teachersSubjects.Select(x => x.User).ToList();
                subjectWithTeachers.Teachers = _mapper.Map<List<TeacherDto>>(users);

                return subjectWithTeachers;
            }
        }

        public async Task<List<AcademicSubject>> ExceptRepeatedSubjects(List<AcademicSubject> assignedSubjects)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var allSubjects = await unitOfWork.AcademicSubjectRepository.GetAllAsync();

                foreach (var subj in assignedSubjects)
                {
                    allSubjects = allSubjects.Where(x => x.Id != subj.Id).ToList();
                }

                return allSubjects;
            }
        }
    }
}
