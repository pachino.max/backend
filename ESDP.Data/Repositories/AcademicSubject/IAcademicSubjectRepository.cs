﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.AcademicSubject
{
    public interface IAcademicSubjectRepository : IRepository<Entities.AcademicSubject>
    {
        Task<Entities.AcademicSubject> GetByIdIncludingAsync(int id);
        Task<IEnumerable<Entities.AcademicSubject>> GetAllIncludingAsync();
    }
}
