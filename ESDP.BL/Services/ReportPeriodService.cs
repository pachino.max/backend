﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.ReportPeriodDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.BL.Services
{
    public class ReportPeriodService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        private readonly IMapper _mapper;

        public ReportPeriodService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public async Task<OperationResult> CreateAsync(ReportPeriodDto reportPeriodDto)
        {
            var reportPeriod = _mapper.Map<ReportPeriodDto, ReportPeriod>(reportPeriodDto);
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var checkReportPeriods = await unitOfWork.ReportPeriodRepository.GetAllAsync();
                if (checkReportPeriods.FirstOrDefault(x => x.Name.ToLower() == reportPeriod.Name.ToLower()) == null)
                {
                    await unitOfWork.ReportPeriodRepository.CreateAsync(reportPeriod);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Отчетный период создан");
                }

                return new OperationResult(false, "Отчетный период с таким именем уже существует");
            }
        }

        public async Task<OperationResult> EditAsync(ReportPeriodDto reportPeriodDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var reportPeriod = await unitOfWork.ReportPeriodRepository.GetByIdAsync(reportPeriodDto.Id);
                var checkReportPeriod = await unitOfWork.ReportPeriodRepository.GetAllAsync();
                if (checkReportPeriod.FirstOrDefault(x => x.Name.ToLower() == reportPeriodDto.Name.ToLower()) == null)
                {
                    reportPeriod = _mapper.Map(reportPeriodDto, reportPeriod);
                    unitOfWork.ReportPeriodRepository.Update(reportPeriod);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Отчетный период изменен");
                }

                return new OperationResult(false, "Отчетный период с таким именем уже существует");
            }
        }

        public async Task<OperationResult> DeleteAsync(int reportPeriodId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var isExist = await unitOfWork.ClassesReportPeriodsRepository.IsExistReportPeriodInClassAsync(reportPeriodId);
                if (!isExist)
                {
                    var reportPeriod = await unitOfWork.ReportPeriodRepository.GetByIdAsync(reportPeriodId);
                    unitOfWork.ReportPeriodRepository.Remove(reportPeriod);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Отчетный период удален");
                }

                return new OperationResult(false,
                    "Удаление невозможно. По данному отчетному периоду существуют классы");
            }
        }

        public async Task<List<ReportPeriodDto>> GetAllReportPeriodsAsync(ReportPeriodFilterDto filterDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var reportPeriods = await unitOfWork.ReportPeriodRepository.GetAllAsync();
                var reportPeriodsViewModels = reportPeriods.Select(_mapper.Map<ReportPeriodDto>).ToList();
                if (filterDto.Name != null)
                {
                    return reportPeriodsViewModels.Where(x =>
                        x.Name.ToLower().Contains(filterDto.Name.ToLower()) && x.IsDeleted == false).ToList();
                }

                return reportPeriodsViewModels.Where(x => x.IsDeleted == false).ToList();
            }
        }

        public async Task<ReportPeriod> GetByIdAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                return await unitOfWork.ReportPeriodRepository.GetByIdAsync(id);
            }
        }

        public List<ReportPeriod> ExceptRepeatedReportPeriods(
            List<ReportPeriod> assignedReportPeriods, List<ReportPeriod> allReportPeriods)
        {
            foreach (var assigned in assignedReportPeriods)
            {
                allReportPeriods = allReportPeriods.Where(x => x.Id != assigned.Id).ToList();
            }

            return allReportPeriods;
        }
    }
}
