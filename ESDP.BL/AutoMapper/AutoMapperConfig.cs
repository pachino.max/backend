﻿using AutoMapper;
using ESDP.BL.AutoMapper.AcademicClassMapper;
using ESDP.BL.AutoMapper.AcademicSubjectMapper;
using ESDP.BL.AutoMapper.AcademicYearMapper;
using ESDP.BL.AutoMapper.ClassDataMapper;
using ESDP.BL.AutoMapper.IdentifierClassMapper;
using ESDP.BL.AutoMapper.ReportPeriodMapper;
using ESDP.BL.AutoMapper.TeacherMapper;
using ESDP.BL.AutoMapper.UserMapper;
using System;
using System.Collections.Generic;
using System.Text;
using ESDP.BL.AutoMapper.AcademicSubjectToTeachersAcademicSubjectsMapper;
using ESDP.BL.AutoMapper.ClassDataMapper.TeacherPageClassDataMapper;
using ESDP.BL.AutoMapper.ClassesReportPeriodsMapper;
using ESDP.BL.AutoMapper.ClassesReportPeriodToAcademicClassMapper;
using ESDP.BL.AutoMapper.GradesMapper;
using ESDP.BL.AutoMapper.TeachersAcademicSubjectsToAcademicSubjectMapper;
using ESDP.BL.AutoMapper.UserToTeacherMapper;

namespace ESDP.BL.AutoMapper
{
    public class AutoMapperConfig
    {
        public MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToUserDtoMappingProfile());
                cfg.AddProfile(new UserDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToAcademicSubjectDtoMappingProfile());
                cfg.AddProfile(new AcademicSubjectDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToIdentifierClassMappingProfile());
                cfg.AddProfile(new CreateIdentifierClassDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToReportPeriodDtoMappingProrile());
                cfg.AddProfile(new ReportPeriodDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToAcademicYearDtoMappingProfile());
                cfg.AddProfile(new AcademicYearDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToTeacherDtoMappingProfile());
                cfg.AddProfile(new TeacherDtoToDomainMappingProfile());
                cfg.AddProfile(new AcademicClassDtoToDomainMappingProfile());
                cfg.AddProfile(new UserToTeacherMappingProfile());
                cfg.AddProfile(new AcademicClassToAcademicClassManagerMappingProfile());
                cfg.AddProfile(new AddReportPeriodToClassDtoToDomainMappingProfile());
                cfg.AddProfile(new ClassesReportPeriodsToAcademicClassReportPeriodManagerMappingProfile());
                cfg.AddProfile(new DomainToClassDataDtoMappingProfile());
                cfg.AddProfile(new AcademicClassToAcademicClassReportPeriodDtoMappingProfile());
                cfg.AddProfile(new AcademicSubjectToSubjectWithTeachersMappingProfile());
                cfg.AddProfile(new UserToTeacherDtoMappingProfile());
                cfg.AddProfile(new TeachersAcademicSubjectsToAcademicSubjectDtoMappingProfile());
                cfg.AddProfile(new ClassDataToTeacherPageClassDataMappingProfile());
                cfg.AddProfile(new DomainToGradesDtoMappingProfile());
            });
        }
    }
}
