﻿using AutoMapper;
using ESDP.Common.Dtos.AcademicClassDtos;
using ESDP.Data.Entities;

namespace ESDP.BL.AutoMapper.ClassesReportPeriodToAcademicClassMapper
{
    public class ClassesReportPeriodsToAcademicClassReportPeriodManagerMappingProfile : Profile
    {
        public ClassesReportPeriodsToAcademicClassReportPeriodManagerMappingProfile()
        {
            CreateAcademicClassReportPeriodManager();
        }

        public void CreateAcademicClassReportPeriodManager()
        {
            CreateMap<ClassesReportPeriods, AcademicClassPeriodManager>()
                .ForMember(src => src.ReportPeriodName, opt => opt.MapFrom(x => x.ReportPeriod.Name));
        }
    }
}
