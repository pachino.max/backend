﻿using System;
using System.Collections.Generic;
using System.Text;
using ESDP.Common.Dtos.AcademicSubjectDtos;

namespace ESDP.Common.Dtos.TeachersDtos
{
    public class TeacherManagerDto
    {
        public string Id { get; set; }
        public string Fullname { get; set; }
        public IList<string> SubjectsName { get; set; }
        public bool IsDeleted { get; set; }

        public IList<AcademicSubjectDto> AssignedSubjects { get; set; }

    }
}
