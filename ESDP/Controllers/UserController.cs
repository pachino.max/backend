﻿using ESDP.BL;
using ESDP.Common.Dtos.UserDtos;
using ESDP.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using ESDP.Common;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Returns the user.
        /// </summary>
        /// <remarks>
        /// Returns the user by its id.
        /// Example of sending a request:
        /// 
        ///     GET /api/user/1
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>User.</returns>
        /// <response code="200">Returns code 200 and the id found by id.</response>
        /// <response code="500">Returns code 500 if the sent id is incorrect.</response>
        [Route("api/user/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetUserAsync(string id)
        {
            if(id != null)
            {
                return Ok(await _userService.FindByIdEditModelAsync(id));
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Returns all roles in system.
        /// </summary>
        /// <remarks>
        /// Returns all roles in system.
        /// 
        /// Example of sending a request:
        /// 
        ///     GET /api/roles
        ///     
        /// </remarks>
        /// <returns>Roles.</returns>
        /// <response code="200">Returns code 200 and a list of roles found.</response>
        [Route("api/roles")]
        [HttpGet]
        public ActionResult GetRoles()
        {
            return Ok(_userService.GetAllRoles());
        }

        /// <summary>
        /// Create new user.
        /// </summary>
        /// <remarks>
        /// Creating a user from existing users with the role of a teacher and assigning subjects to him based on the Dto-model, not all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/add
        ///     {
        ///         "name": "Иван",
        ///         "surname": "Петров",
        ///         "middleName": "Акакиевич",
        ///         "roles": ["Director", "Teacher"],
        ///         "email": "examplemail@example.com",
        ///         "password": "Asd123"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>User created.</returns>
        /// <response code="200">Returns code 200 and the message about the creation of the user.</response>
        /// <response code="500">Returns code 500 if the model is invalid or code 500 and a message stating that a user with the same email already exists.</response>
        /// <response code="400">Returns code 400 if the model is invalid.</response>
        [Route("api/add")]
        [HttpPost]
        public async Task<ActionResult> InsertUser([FromBody] RegisterDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.AddUserAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(400, result);
            }
            return StatusCode(400, new OperationResult(false, "Проверьте правильность введенных данных"));
        }

        /// <summary>
        /// Returns all users.
        /// </summary>
        /// <remarks>
        /// Returns all users, or filtered by fullname and their roles.
        /// The filter works case-insensitive and for incomplete content.
        /// Example of sending a request:
        /// 
        ///     GET /api/usermanager
        ///     {
        ///         "fullname": "Иван Петров",
        ///         "selectedRole": "Teacher"
        ///     }
        ///     
        /// </remarks>
        /// <param name="filter"></param>
        /// <returns>Users.</returns>
        /// <response code="200">Returns code 200 and a list of users found.</response>
        [Route("api/usermanager")]
        [HttpGet]
        public async Task<IActionResult> GetAllUsers(UserFilterDto filter)
        {
            var userViewModels = await _userService.GetAllUsers(filter);
            return Ok(userViewModels);
        }

        /// <summary>
        /// Deletes the selected user.
        /// </summary>
        /// <remarks>
        /// Deletes the selected user by its id.
        /// Example of sending a request:
        /// 
        ///     POST /api/delete
        ///     {
        ///         "id": "cc33539f-7b3f-4b8f-929b-54ec9a1619cf"
        ///     }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Teacher deleted.</returns>
        /// <response code="200">Returns code 200 and the message about the removal of the user.</response>
        /// <response code="400">Returns code 400 if the sent id is not correct.</response>
        [Route("api/delete")]
        [HttpPost]
        public async Task<ActionResult> DeleteUser(string id)
        {
            var result = await _userService.DeleteUserAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(400, result);
        }

        /// <summary>
        /// Blocks or unblocks the selected user.
        /// </summary>
        /// <remarks>
        /// Blocks or unblocks the selected user by its id.
        /// Example of sending a request:
        /// 
        ///     POST /api/blockunblock
        ///     {
        ///         "id": "cc33539f-7b3f-4b8f-929b-54ec9a1619cf"
        ///     }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Teacher deleted.</returns>
        /// <response code="200">Returns code 200 and the message about the block/unblock of the user.</response>
        /// <response code="400">Returns code 400 if the sent id is not correct.</response>
        [Route("api/blockunblock")]
        [HttpPost]
        public async Task<ActionResult> BlockUnblockUser(string id)
        {
            var result = await _userService.BlockUnblockUserAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(400, result);
        }

        /// <summary>
        /// Edits the selected user.
        /// </summary>
        /// <remarks>
        /// Edits the selected user based on the Dto-model, not all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/edit
        ///     {
        ///         "id": "cc33539f-7b3f-4b8f-929b-54ec9a1619cf",
        ///         "name": "Петр",
        ///         "surname": "Иванов",
        ///         "middleName": "Зигмундович",
        ///         "roles": ["Administrator", "Teacher"],
        ///         "email": "examplemail@example.com",
        ///         "password": "Asd123"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Teacher modified.</returns>
        /// <response code="200">Returns code 200 and a message stating that the changes have been saved.</response>
        /// <response code="500">Returns code 500 if the model is invalid or code 500 and a message stating that a user with the same email already exists.</response>
        /// <response code="400">Returns code 400 if the model is invalid.</response>
        [Route("api/edit")]
        [HttpPost]
        public async Task<ActionResult> EditUser([FromBody] UserEditDto model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userService.FindByEmailAsync(model.Email);
                IList<string> roles = null;
                if(user != null)
                {
                   roles = await _userService.GetRolesAsync(user);
                }
                if(user == null || user.Email != model.Email ||
                    user.Name !=model.Name || user.Surname != model.Surname 
                    || user.MiddleName != model.MiddleName || roles != model.Roles)
                {
                    var result = await _userService.EditUserAsync(model);
                    return result.Result ? StatusCode(200, result) : StatusCode(400, result);
                }
            }
            return StatusCode(400, new OperationResult(false, "Введены неверные данные"));
        }
    }
}
