﻿using System;
using System.Collections.Generic;
using System.Text;
using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class ClassesReportPeriodsConfiguration : IEntityTypeConfiguration<ClassesReportPeriods>
    {

        public void Configure(EntityTypeBuilder<ClassesReportPeriods> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Class).WithMany(x => x.ClassesReportPeriods).HasForeignKey(x => x.ClassId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.ReportPeriod).WithMany(x => x.ClassesReportPeriods).HasForeignKey(x => x.ReportPeriodId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
