﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class AcademicClass : Entity
    {
        public int IdentifierClassId { get; set; }
        public IdentifierClass Identifier { get; set; }

        public string ClassroomTeracherId { get; set; }
        public User ClassroomTeacher { get; set; }

        public int YearId { get; set; }
        public AcademicYear Year { get; set; }
        
        public IList<ClassesReportPeriods> ClassesReportPeriods { get; set; }
    }
}
