﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.TeachersDtos
{
    public class TeacherFilterDto
    {
        public string Fullname { get; set; }
        public string SelectedSubject { get; set; }
    }
}
