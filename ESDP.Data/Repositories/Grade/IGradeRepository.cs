﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ESDP.Data.Entities;

namespace ESDP.Data.Repositories.Grade
{
    public interface IGradeRepository : IRepository<Entities.Grades>
    {
        Task<bool> IsExistByClassDataIdAsync(int classDataId);
        Task<Grades> GetByClassDataIdAsync(int classDataId);
    }
}
