﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class ReportPeriod : Entity
    {
        public string Name { get; set; }
        
        public IList<ClassesReportPeriods> ClassesReportPeriods { get; set; }
    }
}
