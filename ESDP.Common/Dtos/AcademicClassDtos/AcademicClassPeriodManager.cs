﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.AcademicClassDtos
{
    public class AcademicClassPeriodManager : BaseEntityDto
    {
        public string ReportPeriodName { get; set; }

        public IList<string> TeachersWithSubjects { get; set; }
    }
}
