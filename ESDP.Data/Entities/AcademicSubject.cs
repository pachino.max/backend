﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class AcademicSubject : Entity
    {
        public string Name { get; set; }

        public ICollection<TeachersAcademicSubjects> Teachers { get; set; }
        public ICollection<ClassData> ClassDatas { get; set; }
    }
}
