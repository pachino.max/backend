﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ESDP.Common.Dtos.TeachersDtos;
using ESDP.Common.Dtos.UserDtos;

namespace ESDP.BL.AutoMapper.UserToTeacherMapper
{
    public class UserToTeacherMappingProfile : Profile
    {
        public UserToTeacherMappingProfile()
        {
            CreateFromUserManagerDtoToTeacherManagerDto();
        }

        public void CreateFromUserManagerDtoToTeacherManagerDto()
        {
            CreateMap<UserManagerDto, TeacherManagerDto>();
        }
    }
}
