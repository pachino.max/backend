﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace ESDP.Data.Repositories.AcademicClass
{
    public class AcademicClassRepository : Repository<Entities.AcademicClass>, IAcademicClassRepository
    {
        public AcademicClassRepository(ApplicationDbContext context) : base(context)
        {
        }


        public async Task<Entities.AcademicClass> GetByIdIncludingAsync(int id)
        {
            return await _context.Classes
                .Include(x => x.Identifier)
                .Include(x => x.ClassroomTeacher)
                .Include(x => x.Year)
                .Include(x=>x.ClassesReportPeriods)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
        
        public async Task<Entities.AcademicClass> GetByIdentifierAndYearIdAsync(int identifierClassId, int yearId)
        {
            return await _context.Classes.FirstOrDefaultAsync(x => x.IdentifierClassId == identifierClassId
                                                                   && x.YearId == yearId);
        }

        public async Task<bool> IsExistIdentifierClassId(int identifierClassId)
        {
            var academicClass =
                await _context.Classes.FirstOrDefaultAsync(x => x.IdentifierClassId == identifierClassId);
            return academicClass != null;
        }

        public async Task<IEnumerable<Entities.AcademicClass>> GetAllIncludingAsync()
        {
            return await _context.Classes
                .Include(x => x.Identifier)
                .Include(x => x.ClassroomTeacher)
                .Include(x => x.Year).ToListAsync();
        }
    }
}
