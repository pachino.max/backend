﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.ClassesReportPeriods
{
    public interface IClassesReportPeriodsRepository : IRepository<Entities.ClassesReportPeriods>
    {

        Task<List<Entities.ClassesReportPeriods>> GetByClassIdIncludingAll(int classId);
        Task<bool> IsExistReportPeriodInClassAsync(int classId, int reportPeriodId);
        Task<bool> IsExistReportPeriodInClassAsync(int reportPeriodId);
        Task<Entities.ClassesReportPeriods> GetByIdIncludingAllAsync(int classReportPeriodId);
        Task CreateRangeAsync(List<Entities.ClassesReportPeriods> classesReportPeriods);
    }
}