﻿using AutoMapper;
using ESDP.Common.Dtos.ReportPeriodDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.ReportPeriodMapper
{
    public class ReportPeriodDtoToDomainMappingProfile : Profile
    {
        public ReportPeriodDtoToDomainMappingProfile()
        {
            CreateReportPeriodDtoMap();
        }

        public void CreateReportPeriodDtoMap()
        {
            CreateMap<ReportPeriodDto, ReportPeriod>();
        }
    }
}
