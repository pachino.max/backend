﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.AcademicClass
{
    public interface IAcademicClassRepository : IRepository<Entities.AcademicClass>
    {

        Task<Entities.AcademicClass> GetByIdentifierAndYearIdAsync(int identifierClassId, int yearId);
        Task<Entities.AcademicClass> GetByIdIncludingAsync(int id);
        Task<bool> IsExistIdentifierClassId(int identifierClassId);
        Task<IEnumerable<Entities.AcademicClass>> GetAllIncludingAsync();
    }
}
