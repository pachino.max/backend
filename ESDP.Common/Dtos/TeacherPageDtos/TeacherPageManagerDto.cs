﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.TeacherPageDtos
{
    public class TeacherPageManagerDto
    {
        public string TeacherId { get; set; }

        public IList<TeacherPageClassDataDto> TeachersClassesWithSubjectsDto { get; set; }

        public TeacherPageManagerDto()
        {
        }

        public TeacherPageManagerDto(string teacherId, IList<TeacherPageClassDataDto> teachersClassesWithSubjectsDto)
        {
            TeacherId = teacherId;
            TeachersClassesWithSubjectsDto = teachersClassesWithSubjectsDto;
        }
    }

}
