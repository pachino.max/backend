﻿using ESDP.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Repositories.IdentifierClass
{
    public class IdentifierClassRepository : Repository<Entities.IdentifierClass>, IIdentifierClassRepository
    {
        public IdentifierClassRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
