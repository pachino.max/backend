﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.AcademicClassDtos
{
    public class AcademicClassFilterDto
    {
        public string Fullname { get; set; }
        public string SelectedIdentifier { get; set; }
        public string SelectedYear { get; set; }
    }
}
