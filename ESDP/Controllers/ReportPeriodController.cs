﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.ReportPeriodDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher, Teacher")]
    public class ReportPeriodController : Controller
    {
        private readonly ReportPeriodService _reportPeriodService;
        public ReportPeriodController(ReportPeriodService service)
        {
            _reportPeriodService = service;
        }

        /// <summary>
        /// Create new ReportPeriod.
        /// </summary>
        /// <remarks>
        /// Creates a new reporting period based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/addReportPeriod
        ///     {
        ///         "name": "Первая четверть"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Reporting period created.</returns>
        /// <response code="200">Returns code 200 and the message about the creation of the reporting period.</response>
        /// <response code="500">Returns code 500 if the model is invalid or code 500 and a message stating that such a reporting period already exists.</response>
        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/addReportPeriod")]
        [HttpPost]
        public async Task<IActionResult> CreateReportPeriod([FromBody] ReportPeriodDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _reportPeriodService.CreateAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Edits the selected reporting period.
        /// </summary>
        /// <remarks>
        /// Edits the selected reporting period based on the Dto-model, all fields are required.
        /// Example of sending a request:
        /// 
        ///     POST /api/editReportPeriod
        ///     {
        ///         "id": 1,
        ///         "name": "Вторая четверть"
        ///     }
        ///     
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>Reporting period modified.</returns>
        /// <response code="200">Returns code 200 and a message stating that the changes have been saved.</response>
        /// <response code="500">Returns code 500 if the model is invalid or code 500 and a message stating that such a reporting period already exists.</response>
        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/editReportPeriod")]
        [HttpPost]
        public async Task<IActionResult> EditReportPeriod([FromBody] ReportPeriodDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _reportPeriodService.EditAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        /// <summary>
        /// Deletes the selected reporting period.
        /// </summary>
        /// <remarks>
        /// Deletes the selected reporting period by its id.
        /// Example of sending a request:
        /// 
        ///     POST /api/deleteReportPeriod
        ///     {
        ///         "id": 1
        ///     }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Reporting period deleted.</returns>
        /// <response code="200">Returns code 200 and the message about the removal of the reporting period.</response>
        /// <response code="500">Returns code 500 if the sent id is not correct or code 500 and a message stating that the reporting period cannot be deleted.</response>
        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/deleteReportPeriod")]
        [HttpPost]
        public async Task<IActionResult> DeleteReportPeriod(int id)
        {
            var result = await _reportPeriodService.DeleteAsync(id);
            return result.Result ? StatusCode(200, result) : StatusCode(500, result);
        }

        /// <summary>
        /// Returns all reporting periods.
        /// </summary>
        /// <remarks>
        /// Returns all reporting periods, or filtered by name.
        /// The filter works case-insensitive and for incomplete content.
        /// Example of sending a request:
        /// 
        ///     GET /api/reportPeriodManager
        ///     {
        ///         "name": "Вторая четверть"
        ///     }
        ///     
        /// </remarks>
        /// <param name="filterDto"></param>
        /// <returns>Reporting Periods.</returns>
        /// <response code="200">Returns code 200 and a list of reporting periods found.</response>
        [Authorize(Roles= "Teacher, Administrator, Director, Headteacher")]
        [Route("api/reportPeriodManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllAcademicYears(ReportPeriodFilterDto filterDto)
        {
            var academicSubjectViewModels = await _reportPeriodService.GetAllReportPeriodsAsync(filterDto);
            return Ok(academicSubjectViewModels);
        }

        /// <summary>
        /// Returns the reporting period.
        /// </summary>
        /// <remarks>
        /// Returns the reporting period by its id.
        /// Example of sending a request:
        /// 
        ///     GET /api/reportPeriod/1
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Reporting period.</returns>
        /// <response code="200">Returns code 200 and the reporting period found by id.</response>
        /// <response code="500">Returns code 500 if the sent id is incorrect.</response>
        [Authorize(Roles = "Administrator, Director, Headteacher")]
        [Route("api/reportPeriod/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetReportPeriodAsync(int id)
        {
            return Ok(await _reportPeriodService.GetByIdAsync(id));
        }
    }
}
