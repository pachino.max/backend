﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.ClassData
{
    public interface IClassDataRepository : IRepository<Entities.ClassData>
    {
        Task<List<Entities.ClassData>> GetByClassesReportPeriodsIdIncludingAllAsync(int classReportPeriodId);
        Task<bool> IsExistClassDataInClassAsync(Entities.ClassData classData);
        Task CreateRangeAsync(List<Entities.ClassData> classData);
        Task<Entities.ClassData> GetByIdIncludingGradesAndSubjectAsync(int classesDataId);
        Task<bool> IsExistTeacherSubjectAsync(int subjectId, string userId);
        Task<List<Entities.ClassData>> GetByTeacherIdIncludingSubjectAcademicClassReportPeriodAsync(string teacherId);
    }
}
