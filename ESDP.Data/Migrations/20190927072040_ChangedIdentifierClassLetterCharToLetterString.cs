﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ESDP.Data.Migrations
{
    public partial class ChangedIdentifierClassLetterCharToLetterString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Letter",
                table: "IdentifierClasses",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Letter",
                table: "IdentifierClasses",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
