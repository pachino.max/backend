﻿using System.Collections.Generic;
using ESDP.Common.Dtos.ReportPeriodDtos;

namespace ESDP.Common.Dtos.AcademicClassDtos
{
    public class AcademicClassReportPeriodManagerDto : AcademicClassManagerDto
    {
        public IList<ReportPeriodDto> ReportPeriodDtos { get; set; }
    }
}